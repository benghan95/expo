const introItems = [
  {
    id: 1,
    imageUri: require('./../../assets/images/Splash01.png'),
    title   : 'Stay organied',
    text    : 'Easily keep track all your warranties, no\nmatter online shopping, or offline purchase.',
  },
  {
    id: 2,
    imageUri: require('./../../assets/images/Splash02.png'),
    title   : 'Flexible protection',
    text    : 'Extend your product warranty or get\nadditional protection with affordable price.',
  },
  {
    id: 3,
    imageUri: require('./../../assets/images/Splash03.png'),
    title   : 'One-touch selling',
    text    : 'Publish and sell your used goods to\nmajor marketplace like Mudah,\nCarousell, Lelong in one-go.',
  },
  {
    id: 4,
    imageUri: require('./../../assets/images/Splash04.png'),
    title   : 'Easy claim',
    text    : 'Easily report a claim with just\na few taps on your phone.',
  },
];

export default introItems;
