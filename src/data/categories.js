export const categories = [
  {
    id       : 1,
    label    : 'Air Conditioner',
    slug     : 'air-conditioner',
    icon     : require('./../../assets/icons/categories/AIR-CONDITIONER-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/AIR-CONDITIONER.png'),
  },
  {
    id       : 2,
    label    : 'Audio',
    slug     : 'audio',
    icon     : require('./../../assets/icons/categories/AUDIO-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/AUDIO.png'),
  },
  {
    id       : 3,
    label    : 'Automobile',
    slug     : 'automobile',
    icon     : require('./../../assets/icons/categories/AUTOMOBILE-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/AUTOMOBILE.png'),
  },
  {
    id       : 4,
    label    : 'Camera',
    slug     : 'camera',
    icon     : require('./../../assets/icons/categories/CAMERA-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/CAMERA.png'),
  },
  {
    id       : 5,
    label    : 'Furniture',
    slug     : 'furniture',
    icon     : require('./../../assets/icons/categories/FURNITURE-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/FURNITURE.png'),
  },
  {
    id       : 6,
    label    : 'Game Console',
    slug     : 'game-console',
    icon     : require('./../../assets/icons/categories/GAME-CONSOLE-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/GAME-CONSOLE.png'),
  },
  {
    id       : 7,
    label    : 'Kitchen',
    slug     : 'kitchen',
    icon     : require('./../../assets/icons/categories/KITCHEN-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/KITCHEN.png'),
  },
  {
    id       : 8,
    label    : 'Laptop',
    slug     : 'laptop',
    icon     : require('./../../assets/icons/categories/LAPTOP-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/LAPTOP.png'),
  },
  {
    id       : 9,
    label    : 'Laundry',
    slug     : 'laundry',
    icon     : require('./../../assets/icons/categories/LAUNDRY-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/LAUNDRY.png'),
  },
  {
    id       : 10,
    label    : 'Other',
    slug     : 'other',
    icon     : require('./../../assets/icons/categories/OTHER-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/OTHER.png'),
  },
  {
    id       : 11,
    label    : 'Phone',
    slug     : 'phone',
    icon     : require('./../../assets/icons/categories/PHONE-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/PHONE.png'),
  },
  {
    id       : 12,
    label    : 'Tablet',
    slug     : 'tablet',
    icon     : require('./../../assets/icons/categories/TABLET-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/TABLET.png'),
  },
  {
    id       : 13,
    label    : 'TV',
    slug     : 'tv',
    icon     : require('./../../assets/icons/categories/TV-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/TV.png'),
  },
  {
    id       : 14,
    label    : 'Watch',
    slug     : 'watch',
    icon     : require('./../../assets/icons/categories/WATCH-blue.png'),
    whiteIcon: require('./../../assets/icons/categories/WATCH.png'),
  },
];
