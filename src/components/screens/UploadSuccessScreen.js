import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image } from 'react-native';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { Button, Icon } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import Header from 'components/common/CustomHeader';
import CloseButton from 'components/common/CloseButton';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #234E7F;
`;

const ContentContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
`;

const IconContainer = styled.View`
marginBottom: 50;
`;

const Title = styled.Text`
  color            : #fff;
  fontFamily       : 'gotham-book';
  fontSize         : 26;
  textAlign        : center;
  paddingHorizontal: 20;
  marginBottom     : 20;
`;

const Subtitle = styled.Text`
  color            : #fff;
  fontFamily       : 'gotham-book';
  fontSize         : 14;
  textAlign        : center;
  paddingHorizontal: 20;
  marginBottom     : 70;
`;

const BackMainText = styled.Text`
  color            : #fff;
  fontFamily       : 'gotham-bold';
  fontSize         : 24;
  marginBottom     : 30;
  paddingHorizontal: 20;
  textAlign        : center;
`;

const AddMoreContainer = styled.View`
  opacity       : 0.5;
  alignItems    : center;
  justifyContent: center;
`;

const CTATitle = styled.Text`
  color            : #fff;
  fontFamily       : 'gotham-book';
  fontSize         : 12;
  textAlign        : center;
  paddingHorizontal: 20;
`;

const styles = StyleSheet.create({
  buttonText: {
    color     : '#234E7F',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'gotham-bold'
  },
  button: {
    paddingTop   : 18,
    paddingBottom: 16,
    width        : 200,
    marginBottom : 8,
  },
})

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}

class Screen extends Component {
  state = {
    isReady: true,
    receipt: null,
  }

  async _loadAssetsAsync() {
    // const imageAssets = cacheImages([
    //   require('./assets/images/circle.jpg'),
    // ]);

    // const fontAssets = cacheFonts([FontAwesome.font]);

    // await Promise.all([...fontAssets]);
    // await Promise.all([...imageAssets, ...fontAssets]);
  }

  constructor(props){
    super(props);
  }

  _resetUpload = () => {
    this.setState({
      receipt: null,
    })
  }

  _submitReceipt = () => {
    if (!this.state.receipt){
      Alert.alert(
        "Please ensure that you have uploaded your receipt."
      )
    }
  }

  render() {
    let { receipt } = this.state;

    if (!this.state.isReady) {
      return (
        <AppLoading
          // startAsync = {this._loadAssetsAsync}
          // onFinish   = {() => this.setState({ isReady: true })}
          // onError    = {console.warn}
        />
      );
    }

    return (
      <Container>
        <Header type="default" bgColor="#234E7F" navigation={this.props.navigation} leftComponent={null} />
        <ContentContainer>
          <CloseButton onPress = {() => this.props.navigation.navigate("Home")} />
          <IconContainer>
            <Image
              source = {require('./../../../assets/icons/default/white/Done-icon.png')}
              style  = {{alignSelf: 'center', width: 70, height: 70, resizeMode: 'contain'}}
            />
          </IconContainer>
          <Title>UPLOAD SUCCESS</Title>
          <Subtitle>Your record is successfully uploaded and{'\n'}available in next *24 hours after verified.
            {'\n'}
            {'\n'}* working hours excluded Public Holiday,{'\n'}Saturday and Sunday.
          </Subtitle>
          <TouchableOpacity onPress = { () => this.props.navigation.navigate("Home") }>
            <BackMainText>BACK TO MAIN</BackMainText>
          </TouchableOpacity>
          <AddMoreContainer>
            <Icon
              name           = 'plus'
              type           = 'font-awesome'
              color          = '#fff'
              component      = { TouchableOpacity }
              containerStyle = {{
                borderStyle : 'dotted',
                borderRadius: 100,
                borderWidth : 2,
                borderColor : '#fff',
                marginBottom: 13,
              }}
              iconStyle = {{paddingVertical: 23, paddingHorizontal: 25}}
              onPress   = { () => this.props.navigation.navigate("UploadMethod") }
              size      = {18}
            />
            <CTATitle>Add More</CTATitle>
          </AddMoreContainer>
        </ContentContainer>
      </Container>
    );
  }
}

export default Screen;