import React, { Component } from 'react';
import { Alert, Animated, Platform, TouchableOpacity, Text, StyleSheet, View, Image, ScrollView, Dimensions, AsyncStorage, RefreshControl } from 'react-native';
import Expo, { Font, AppLoading, Asset, Constants } from 'expo';
import { Button, Icon } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Header from 'components/common/CustomHeader';
import Loader from 'components/common/Loader';
// import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import DetailScene from 'components/scenes/DetailScene';
import SellScene from 'components/scenes/SellScene';
import ProtectionScene from 'components/scenes/ProtectionScene';
import Tabs from 'react-native-tabs';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const BannerContainer = styled.View`
  marginTop     : -70;
  justifyContent: center;
  alignItems    : center;
  position      : relative;
  height        : 220;
  width         : 100%;
`;

const ProgressContainer = styled.View`
  borderRadius  : 100;
  width         : 200;
  alignItems    : center;
  justifyContent: center;
  position      : relative;
`;

const ProgressBorder = styled.View`
  height         : 80;
  width          : 80;
  borderStyle    : dotted;
  borderRadius   : 100;
  borderWidth    : 4;
  borderColor    : #E0E2E1;
  backgroundColor: #fff;
  left           : 0;
  top            : 0;
  position       : absolute;
  zIndex         : 5;
`;

const ProgressCategoryImage = styled.Image`
  alignSelf: center;
  width    : 40;
  height   : 40;
  position : absolute;
  resizeMode: contain;
`;

const ContentContainer = styled.View`
  paddingTop: 60;
`;

const TabLabel = styled.Text`
  color     : #000;
  fontSize  : 14;
  fontFamily: 'gotham-bold';
`;

const ProductTitle = styled.Text`
  color       : #4D4D4F;
  fontFamily  : 'gotham-book';
  fontSize    : 20;
  textAlign   : center;
  marginBottom: 60;
`;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  progressBar: {
    backgroundColor: '#fff',
    borderRadius   : 80,
    transform      : [{scaleX: -1}],
  },
  tab: {
    backgroundColor  : '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    paddingHorizontal: 5,
  },
  activeLabel: {
    borderBottomWidth: 3,
    borderBottomColor: '#1FA5EE',
    marginBottom     : -1,
  },
  indicator: {
    borderBottomColor: '#1FA5EE',
    borderBottomWidth: 2,
    alignSelf        : 'center',
    // width: '100%',
  },
  // tabbar: {
  //   paddingHorizontal: 0,
  // }
})

// const initialLayout = {
//   height: 0,
//   width : Dimensions.get('window').width,
// };

// const DetailRoute     = () => <DetailScene />;
// const SellRoute       = () => <SellScene />;
// const ProtectionRoute = () => <ProtectionScene />;

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady   : true,
      refreshing: false,
      page      : null,
      loading   : false,
      entry     : null,
      data      : null,
      isAdmin   : false,
    };
  }

  componentWillMount() {
    this.setState({
      page: 'detail',
    });

    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params.tab === 'detail') {
        this.setState({
          page: 'detail',
        });
      } else if (this.props.navigation.state.params.tab === 'sell') {
        this.setState({
          page: 'sell',
        });
      } else if (this.props.navigation.state.params.tab === 'protection') {
        this.setState({
          page: 'protection',
        });
      }
    }
  }

  componentDidMount() {
    this._retrieveEntry();
  }

  _onRefresh = () => {
    this._retrieveEntry();
  }

  _retrieveEntry = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem('Kepp.jwtToken');

    const { data } = this.props.navigation.state.params;
    this.setState({ data });
    if (data.itemId) {
      fetch(`https://www.kepp.link/secure/item/${data.itemId}`, {
        method : 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => response.json())
        .then((responseJson) => {
          this.setState({ loading: false });
          this.setState({ entry: responseJson });
        })
        .catch((err) => {
          this.setState({ loading: false });
          Alert.alert(JSON.stringify(err));
        });
    }

    this.setState({ loading: true });
    try {
      if (token) {
        fetch('https://www.kepp.link/secure/userdetail', {
          method : 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
          .then(response => response.json())
          .then(async (responseJson) => {
            this.setState({ loading: false });
            if (responseJson.statusResp) {
              const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
              if (responseCode >= 100000 && responseCode < 200000) {
                if (responseJson.isAdmin) {
                  this.setState({
                    isAdmin: true,
                  });
                }
              } else {
                await AsyncStorage.removeItem('Kepp.jwtToken');
                this.props.navigation.navigate('Login');
              }
            } else {
              await AsyncStorage.removeItem('Kepp.jwtToken');
              this.props.navigation.navigate('Login');
            }
          })
          .catch(async () => {
            await AsyncStorage.removeItem('Kepp.jwtToken');
            this.setState({ loading: false });
            this.props.navigation.navigate('Login');
            // Alert.alert(JSON.stringify(err));
          });
      } else {
        this.setState({ loading: false });
        this.props.navigation.navigate('Login');
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err));
    }
  }

  render() {
    return (
      <Container>
        <Loader loading={this.state.loading} />
        <ScrollView
          refreshControl        = {
            <RefreshControl
              refreshing = {this.state.refreshing}
              onRefresh  = {this._onRefresh.bind(this)}
            />
          }
        >
          <Header type="default" bgColor="transparent" navigation={this.props.navigation} leftComponent outerContainerStyles={{zIndex: 999, position: 'relative'}} />
          <BannerContainer>
            <Image
              source = {this.state.entry ? { uri: `https://www.kepp.link/unsecure/getimage/${this.state.entry.productImage}` } : null}
              style  = {{ backgroundColor: '#ccc', width: '100%', height: 220, resizeMode: 'cover' }}
            />
            <View style={{ position: 'absolute', bottom: -40, justifyContent: 'center', alignItems: 'center' }}>
              <ProgressContainer>
                <View style={styles.progressBar}>
                  <ProgressBorder />
                  <AnimatedCircularProgress
                    rotation        = {0}
                    arcSweepAngle   = {360}
                    size            = {80}
                    style           = {{ backgroundColor: 'transparent', zIndex: 10 }}
                    width           = {4}
                    fill            = {this.state.data ? this.state.data.percentage : null}
                    linecap         = "round"
                    tintColor       = "#4A90E2"
                    backgroundColor = "#E0E2E1"
                  />
                </View>
                <ProgressCategoryImage
                  source = {this.state.data ? this.state.data.category.icon : null}
                />
              </ProgressContainer>
            </View>
          </BannerContainer>

          <ContentContainer>
            <ProductTitle>{this.state.entry ? this.state.entry.itemShortName : '-'}</ProductTitle>
            <View>
              <Tabs
                selected          = {this.state.page}
                style             = {styles.tab}
                selectedIconStyle = {styles.activeLabel}
                onSelect          = {el=>this.setState({page:el.props.name})}>
                <TabLabel name="detail">DETAIL</TabLabel>
                <TabLabel style={{ color: '#ccc' }} name="sell">SELL</TabLabel>
                <TabLabel style={{ color: '#ccc' }} name="protection">PROTECTION</TabLabel>
              </Tabs>
            </View>
            {
              this.state.page === 'sell' ? <SellScene data={this.state.data} entry={this.state.entry} isAdmin={this.state.isAdmin} /> : null
            }
            {
              this.state.page === 'protection' ? <ProtectionScene data={this.state.data} entry={this.state.entry} isAdmin={this.state.isAdmin} /> : null
            }
            {
              this.state.page === 'detail' ? <DetailScene data={this.state.data} entry={this.state.entry} navigation={this.props.navigation} isAdmin={this.state.isAdmin} /> : null
            }
            {/* <TabViewAnimated
              navigationState = {this.state}
              renderScene     = {this._renderScene}
              renderHeader    = {this._renderHeader}
              onIndexChange   = {this._handleIndexChange}
            /> */}
            {/* <TabViewAnimated
              style           = {styles.container}
              navigationState = {this.state}
              renderScene     = {this._renderScene}
              renderHeader    = {this._renderHeader}
              onIndexChange   = {this._handleIndexChange}
              // initialLayout   = {initialLayout}
            /> */}
          </ContentContainer>
        </ScrollView>
      </Container>
    );
  }
}

export default Screen;
