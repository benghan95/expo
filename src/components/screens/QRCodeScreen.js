import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image, AsyncStorage } from 'react-native';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { Button, Icon } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import Header from 'components/common/CustomHeader';
import QRCode from 'react-native-qrcode';
import Loader from 'components/common/Loader';
import CloseButton from 'components/common/CloseButton';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #234E7F;
`;

const ContentContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
`;

const Title = styled.Text`
  color            : #fff;
  fontFamily       : 'gotham-book';
  fontSize         : 15;
  marginTop        : -70;
  textAlign        : center;
  paddingHorizontal: 20;
  marginBottom     : 70;
`;

const QRCodeContainer = styled.View`
  backgroundColor  : #fff;
  paddingVertical  : 35;
  paddingHorizontal: 35;
`;


const styles = StyleSheet.create({
})

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}

class Screen extends Component {
  // async _loadAssetsAsync() {
  //   const imageAssets = cacheImages([
  //     require('./assets/images/circle.jpg'),
  //   ]);

  //   const fontAssets = cacheFonts([FontAwesome.font]);

  //   await Promise.all([...fontAssets]);
  //   await Promise.all([...imageAssets, ...fontAssets]);
  // }

  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      loading: false,
      text   : 'https://www.kepp.link/user/',
    };
  }

  async componentDidMount() {
    try {
      const token = await AsyncStorage.getItem('Kepp.jwtToken');
      this.setState({ loading: true });
      if (token) {
        fetch('https://www.kepp.link/secure/userdetail', {
          method : 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
          .then(response => response.json())
          .then((responseJson) => {
            this.setState({loading: false});
            const data = responseJson;
            if ('userId' in data){
              this.setState({
                text: `https://www.kepp.link/user/${data.userId}`
              });
            }
          })
          .catch((err) => {
            this.setState({ loading: false });
            Alert.alert(JSON.stringify(err));
          });
      }
      else{
        this.setState({loading: false});
        // this.props.navigation.navigate('Login');
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err));
    }
  }


  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          // startAsync = {this._loadAssetsAsync}
          // onFinish   = {() => this.setState({ isReady: true })}
          // onError    = {console.warn}
        />
      );
    }

    return (
      <Container>
        <Loader loading = {this.state.loading} />
        <Header type="default" bgColor="#234E7F" navigation={this.props.navigation} leftComponent={null} />
        <ContentContainer>
          <CloseButton onPress={() => this.props.navigation.navigate('UploadMethod')} />
          <Title>Show this QR code at participated{'\n'}outlet, your purchase and warranty{'\n'}info will be automatically uploaded.</Title>
          <QRCodeContainer>
            <QRCode
              value   = {this.state.text}
              size    = {200}
              bgColor = '#000'
              fgColor = '#fff'/>
          </QRCodeContainer>
        </ContentContainer>
      </Container>
    );
  }
}

export default Screen;