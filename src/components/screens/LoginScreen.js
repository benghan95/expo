import React, { Component } from 'react';
import { Alert, Platform } from 'react-native';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { Text, StyleSheet, View, Image, ScrollView } from 'react-native';
import { Button } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import introItems from 'data/data';
import Carousel from 'react-native-looped-carousel-improved';
import Swiper from 'react-native-swiper';
import Loader from 'components/common/Loader';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const Intro = styled.View`
  height: 350px;
`;

const LoginButtons = styled.View`
  ${'' /* flex: 5; */}
  paddingTop: 20px;
  paddingBottom: 50px;
`;

const Title = styled.Text`
  fontFamily  : 'gotham-bold';
  fontSize    : 14;
  marginBottom: 25;
  textAlign   : center;
`;

const IntroTitle = styled.Text`
  color       : #333;
  fontFamily  : 'gotham-bold';
  fontSize    : 14;
  lineHeight  : 18;
  marginBottom: 15;
  textAlign   : center;
`;

const IntroDesc = styled.Text`
  color       : #666;
  fontFamily  : 'gotham-medium';
  fontSize    : 13;
  lineHeight  : 17;
  marginBottom: 10;
  textAlign   : center;
`;

const IntroTitleContainer = styled.View`
`;

const IntroContainer = styled.View`
  flex             : 1;
  backgroundColor  : transparent;
  justifyContent   : center;
  alignItems       : center;
  paddingHorizontal: 30;
`;

const IntroImageContainer = styled.View`
  marginBottom: 15;
`;


const SwiperDot = styled.View`
  backgroundColor: #fff;
  borderColor    : #484848;
  borderWidth    : 1;
  width          : 10;
  height         : 10;
  borderRadius   : 10;
  marginLeft     : 6;
  marginRight    : 6;
`;

const SwiperDotActive = styled.View`
  backgroundColor: #139AD6;
  width          : 13;
  height         : 13;
  borderRadius   : 7;
  marginLeft     : 6;
  marginRight    : 6;
`;

const styles = StyleSheet.create({
  buttonText: {
    color     : '#ffffff',
    fontSize  : 16,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  },
  buttonIcon: {
    ...Platform.select({
      ios: {
        marginRight: 15,
        marginTop  : -2,
      },
      android: {
        marginRight: 15,
        marginTop  : 0,
      },
    }),
  },
  button: {
    paddingTop   : 20,
    paddingBottom: 18,
    marginBottom : 8,
  },
  introImage: {
    width : 120,
    height: 120,
    resizeMode: 'contain',
  },
  // swiper: {
  //   paddingBottom: 10,
  // },
});

// cacheImages = (images) => {
//   return images.map(image => {
//     if (typeof image === 'string') {
//       return Image.prefetch(image);
//     } else {
//       return Asset.fromModule(image).downloadAsync();
//     }
//   });
// }

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}

class LoginScreen extends Component {
  // async _loadAssetsAsync() {
  //   const imageAssets = cacheImages([
  //     require('./assets/images/circle.jpg'),
  //   ]);

  //   const fontAssets = cacheFonts([FontAwesome.font]);

  //   await Promise.all([...fontAssets]);
  //   await Promise.all([...imageAssets, ...fontAssets]);
  // }

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  _fbLogin = async (navigation) => {
    try {
      const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('889946864499425', {
        permissions: ['public_profile', 'email'],
      });

      this.setState({ loading: true });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email`);

        const data   = {};
        const result = await response.json();

        data.id    = result.id;
        data.name  = result.name;
        data.email = result.email;
        data.token = token;

        this.setState({ loading: false });

        navigation.navigate('Welcome', { method: 'facebook', data });
      } else {
        this.setState({ loading: false });
      }
    } catch (err) {
      this.setState({ loading: false });
    }
  }

  _googleLogin = async (navigation) => {
    try {
      const result = await Expo.Google.logInAsync({
        behavior                    : 'system',
        androidClientId             : '821344322429-g8h0ikk1lugp9ceq7q3aa1a9mfjq6fho.apps.googleusercontent.com',
        iosClientId                 : '821344322429-gsk2pqkgh9l9l5acoib61tb4blb1pkrt.apps.googleusercontent.com',
        androidStandaloneAppClientId: '399780154840-hu5r668qmecp8o6qbfkt3vuqcg8n74lh.apps.googleusercontent.com',
        iosStandaloneAppClientId    : '399780154840-oct09lm8ca1p9voc2grphbu9dnlup36a.apps.googleusercontent.com',
        webClientId                 : '399780154840-tlu6siuptu0c6136b8kk4k4pn7044kms.apps.googleusercontent.com',
        scopes                      : ['profile', 'email'],
      });

      this.setState({ loading: true });
      if (result.type === 'success') {
        const data = {};
        this.setState({ loading: false });
        if (result.user) {
          data.id    = result.user.id;
          data.name  = result.user.name;
          data.email = result.user.email;
          data.token = result.accessToken;
          navigation.navigate('Welcome', { method: 'google', data });
        } else {
          Alert.alert('Error occured! Please try another login method.');
        }
      } else if (result.type === 'cancel') {
        Alert.alert('Cancelled Google Login, please try another login method.');
        this.setState({ loading: false });
      } else {
        this.setState({ loading: false });
        Alert.alert('Error occured! Please try another login method.');
      }
    } catch (err) {
      this.setState({ loading: false });
      fetch('https://portal.unlimitedpower.academy/api/v1/debug', {
        method : 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        message: JSON.stringify(err),
      })
        .then(response => response.json())
        .then((responseJson) => {
          console.log(responseJson);
        })
        .catch((error) => {
          this.setState({ loading: false });
          Alert.alert(JSON.stringify(error));
        });
      // Alert.alert('Error', JSON.stringify(err));
    }
  }

  _renderIntroItems = introItems.map(prop => (
    <IntroContainer key={prop.id}>
      <IntroImageContainer>
        <Image
          style      = {styles.introImage}
          source     = { prop.imageUri }
        />
      </IntroImageContainer>
      <IntroTitleContainer>
        <IntroTitle>{prop.title}</IntroTitle>
        <IntroDesc>{prop.text}</IntroDesc>
      </IntroTitleContainer>
    </IntroContainer>
  ));

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading} />
        <ScrollView>
          <Intro>
            <Swiper
              loop
              autoplay
              autoplayDirection
              autoplayTimeout = {3}
              style           = {styles.swiper}
              ref             = {(c) => { this._swiper = c; }}
              index           = {0}
              dot             = {<SwiperDot />}
              activeDot       = {<SwiperDotActive />}
              paginationStyle = {{ bottom: 30 }}
            >
              {this._renderIntroItems}
            </Swiper>
          </Intro>
          <LoginButtons>
            <Title>SIGN IN WITH</Title>
            <Button
              onPress     = {() => this._fbLogin(this.props.navigation)}
              icon        = {{name: 'facebook', type: 'font-awesome', size: 20, style: styles.buttonIcon}}
              buttonStyle = {[{backgroundColor: '#3B99FC'}, styles.button]}
              textStyle   = {styles.buttonText}
              title       = 'FACEBOOK'
              color       = "#841584"
            />
            <Button
              onPress     = {() => this._googleLogin(this.props.navigation)}
              icon        = {{name: 'google', type: 'font-awesome', size: 20, style: styles.buttonIcon}}
              buttonStyle = {[{backgroundColor: '#EC5346'}, styles.button]}
              textStyle   = {styles.buttonText}
              title       = 'GOOGLE'
              color       = "#841584"
            />
            <Button
              onPress     = {() => this.props.navigation.navigate('Welcome', {method: 'mobile'})}
              icon        = {{name: 'phone', type: 'font-awesome', size: 20, style: styles.buttonIcon}}
              buttonStyle = {[{backgroundColor: '#787878'}, styles.button]}
              textStyle   = {styles.buttonText}
              title       = 'MOBILE NO'
              color       = "#841584"
            />
          </LoginButtons>
        </ScrollView>
      </Container>
    );
  }
}

export default LoginScreen;