import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image, AsyncStorage, TouchableHighlight } from 'react-native';
import Expo, { Font, AppLoading, Asset, ImagePicker, ImageManipulator, Permissions } from 'expo';
import { Button, Icon, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import Swiper from 'react-native-swiper';
import Header from 'components/common/CustomHeader';
import Loader from 'components/common/Loader';
import NextButton from 'components/common/NextButton';
import CloseButton from 'components/common/CloseButton';
import Modal from 'react-native-modal';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #234E7F;
`;

const ContentContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
`;

const SectionContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
  marginTop     : -70;
`;

const InputContainer = styled.View`
  marginBottom: 20;
  width       : 100%;
  position    : relative;
`;

const FormContainer = styled.View`
  height           : 300;
  paddingHorizontal: 30;
  width            : 100%;
`;

const FormInputContainer = styled.View`
  position    : relative;
  marginBottom: 10;
`;

const SwiperDot = styled.View`
  backgroundColor: #fff;
  width          : 5;
  height         : 5;
  borderRadius   : 7;
  marginLeft     : 6;
  marginRight    : 6;
`;

const SwiperDotActive = styled.View`
  backgroundColor: #13A1FF;
  width          : 10;
  height         : 10;
  borderRadius   : 7;
  marginLeft     : 6;
  marginRight    : 6;
`;

const ModalOption = styled.TouchableHighlight`
  paddingHorizontal: 15;
  paddingVertical: 15;
`;

const ModalOptionText = styled.Text`
  fontSize    : 18;
  fontFamily  : 'gotham-book';
`;

const Title = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-book';
  fontSize    : 18;
  marginBottom: 35;
  textAlign   : center;
  opacity     : 0.6;
`;

const Bold = styled.Text`
  fontFamily: 'gotham-bold';
`;

const NextContainer = styled.View`
  flexDirection: row;
`;

const NextText = styled.Text`
  color: #fff;
`;

const UploadImage = styled.View`
  alignItems     : center;
  overflow       : hidden;
  justifyContent : center;
  backgroundColor: #D8D8D8;
  borderRadius   : 130;
  height         : 130;
  width          : 130;
  marginBottom   : 20;
`;

// const Title = styled.Text`
//   color            : #fff;
//   fontFamily       : 'gotham-book';
//   fontSize         : 15;
//   marginTop        : -40;
//   textAlign        : center;
//   paddingHorizontal: 20;
//   marginBottom     : 30;
// `;

// const UploadContainer = styled.View`
//   paddingTop   : 20;
//   paddingBottom: 50;
// `;

// const ResetText = styled.Text`
//   color     : #fff;
//   fontFamily: 'gotham-book';
//   fontSize  : 15;
//   textAlign : center;
// `;

// const ConfirmContainer = styled.View`
//   alignItems: center;
// `;

// const UploadReceipt = styled.View`
//   alignItems     : center;
//   overflow       : hidden;
//   justifyContent : center;
//   backgroundColor: #D8D8D8;
//   borderRadius   : 150;
//   height         : 150;
//   width          : 150;
//   marginBottom   : 20;
// `;


const styles = StyleSheet.create({
  // buttonText: {
  //   color     : '#234E7F',
  //   fontSize  : 18,
  //   textAlign : 'center',
  //   fontFamily: 'gotham-bold'
  // },
  input: {
    color     : '#fff',
    fontFamily: 'gotham-bold',
    fontSize  : 24,
    textAlign : 'center',
    ...Platform.select({
      android: {
        paddingBottom: 15,
      },
    }),
    width     : '100%',
  },
  inputContainer: {
    borderBottomColor: 'rgba(255, 255, 255, 0.4)',
    marginBottom     : 5,
    marginLeft       : 35,
    marginRight      : 35,
    paddingVertical  : 5,
    paddingHorizontal: 20,
  },
  inputError: {
    fontSize  : 12,
    textAlign : 'center',
    fontFamily: 'gotham-book',
  },
  button: {
    paddingTop   : 18,
    paddingBottom: 16,
    width        : 200,
    marginBottom : 8,
  },
  buttonIcon: {
    ...Platform.select({
      ios: {
        marginLeft: 5,
        marginTop : -2,
      },
      android: {
        marginLeft: 5,
        marginTop : 0,
      },
    }),
  },
})

// cacheImages = (images) => {
//   return images.map(image => {
//     if (typeof image === 'string') {
//       return Image.prefetch(image);
//     } else {
//       return Asset.fromModule(image).downloadAsync();
//     }
//   });
// }

// cacheFonts = (fonts) => {
//   return fonts.map(font => Font.loadAsync(font));
// }

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading        : false,
      productName    : null,
      receiptImage   : null,
      receiptImageId : null,
      warrantyImage  : null,
      warrantyImageId: null,
      handlingImage  : null,
      showModal      : false,
    };
  }

  // _resetUpload = () => {
  //   this.setState({
  //     receipt: null,
  //   })
  // }

  _nextSlide = () => {
    const { swiper } = this;
    swiper.scrollBy(1);
  }

  _uploadImage = async (token, formData, type, state) => {
    await fetch(`https://www.kepp.link/secure/${type}`, {
      method : 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type' : 'multipart/form-data',
      },
      body: formData,
    })
      .then(response => response.json())
      .then((responseJson) => {
        if (responseJson.statusResp) {
          if (parseInt(responseJson.statusResp.responseCode, 10) >= 100000) {
            this.setState({
              [state]: responseJson.imageId,
            });
          } else {
            Alert.alert(responseJson.statusResp.responseCode);
          }
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  _getFormData = (state) => {
    const formData = new FormData();
    const uri      = this.state[state];
    const uriArr   = uri.split('/');
    const filename = uriArr[uriArr.length - 1];
    const fileArr  = filename.split('.');
    const ext      = fileArr[fileArr.length - 1];
    formData.append('file', {
      uri,
      name: `${filename}`,
      type: `image/${ext}`,
    });

    return formData;
  }

  _processData = async (token) => {
    if (this.state.receiptImage) {
      const receiptImageData = this._getFormData('receiptImage');
      const receiptImageId   = await this._uploadImage(token, receiptImageData, 'uploadreceipt', 'receiptImageId');
    }
    if (this.state.warrantyImage) {
      const warrantyImageData = this._getFormData('warrantyImage');
      const warrantyImageId   = await this._uploadImage(token, warrantyImageData, 'uploadwarrantycard', 'warrantyImageId');
    }
    return this.state;
  }

  _submit = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem('Kepp.jwtToken');

    if (!this.state.productName) {
      Alert.alert('Please enter your product name.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.receiptImage) {
      Alert.alert('Please upload your product receipt.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.warrantyImage) {
      Alert.alert('Please upload your product receipt.');
      this.setState({ loading: false });
      return false;
    }

    const process = await this._processData(token);

    const data = {
      itemShortName    : this.state.productName,
      receiptImage     : this.state.receiptImageId,
      warrantyCardImage: this.state.warrantyImageId,
    };

    fetch('https://www.kepp.link/secure/receiptentry', {
      method : 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type' : 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({ loading: false });
        if ('itemShortName' in responseJson) {
          this.props.navigation.navigate('UploadSuccess');
        } else {
          Alert.alert(JSON.stringify(responseJson));
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  _pickImage = async (param) => {
    this.setState({
      handlingImage: param,
      showModal: true,
    });
  };

  _takePhoto = async (param) => {
    this.setState({
      showModal: false,
    });

    let granted = true;

    const allowedCamera = await Permissions.askAsync(Permissions.CAMERA);

    if (allowedCamera.status !== 'granted') {
      Alert.alert('Warning', 'Please allow KEPP to open your Camera so that you can snap a photo to upload.');
      granted = false;
      return;
    }

    if (Platform.OS === 'ios') {
      const allowedCameraRoll = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (allowedCameraRoll.status !== 'granted') {
        Alert.alert('Warning', 'Please allow KEPP to open your Camera Roll so that you can choose an image to upload.');
        granted = false;
        return;
      }
    }

    // granted = true;

    if (granted) {
      const result = await ImagePicker.launchCameraAsync({
        quality: 0.72,
      }).catch((err) => {
        fetch('https://portal.unlimitedpower.academy/api/v1/debug', {
          method : 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          message: JSON.stringify(err),
        })
          .then(response => response.json())
          .then((responseJson) => {
            console.log(responseJson);
          })
          .catch((error) => {
            this.setState({ loading: false });
            Alert.alert(JSON.stringify(error));
          });
      });

      if (!result.cancelled) {
        const manipResult = await ImageManipulator.manipulate(
          result.uri,
          [{ resize: { width: 480 } }],
          { format: 'jpeg' },
        );
        this.setState({ [param]: manipResult.uri });
      }
    } else {
      Alert.alert('Error', 'Please check your app permission and allow KEPP to open your Camera and Camera Roll to upload an image.');
    }
  }

  _chooseImage = async (param) => {
    this.setState({
      showModal: false,
    });

    let granted = false;
    if (Platform.OS === 'ios') {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

      if (status === 'granted') {
        granted = true;
      } else {
        Alert.alert('Warning', 'Please allow us to open your Camera Roll, or else you will not be able to choose any image.');
        return;
      }
    } else {
      granted = true;
    }

    if (granted) {
      const result = await ImagePicker.launchImageLibraryAsync({
        quality: 0.72,
      });

      if (!result.cancelled) {
        const manipResult = await ImageManipulator.manipulate(
          result.uri,
          [{ resize: { width: 480 } }],
          { format: 'jpeg' },
        );
        this.setState({ [param]: manipResult.uri });
      }
    } else {
      Alert.alert('Error', 'Please check your app permission and allow KEPP to open your Camera Roll to upload an image.');
    }
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading} />
        <Header type="default" bgColor="#234E7F" navigation={this.props.navigation} leftComponent={null} />
        <Modal
          isVisible={this.state.showModal}
          onBackdropPress={() => this.setState({ showModal: false })}
          backdropOpacity={0.5}
        >
          <View style={{ backgroundColor: '#fff', flexWrap: 'wrap', flexDirection: 'column' }}>
            <ModalOption
              onPress={() => this._takePhoto(this.state.handlingImage)}
              underlayColor="#eee"
            >
              <ModalOptionText>Take a photo</ModalOptionText>
            </ModalOption>
            <ModalOption
              onPress={() => this._chooseImage(this.state.handlingImage)}
              underlayColor="#eee"
            >
              <ModalOptionText>Choose Image from Gallery</ModalOptionText>
            </ModalOption>
          </View>
        </Modal>
        <ContentContainer>
          <CloseButton onPress={() => this.props.navigation.navigate('UploadMethod')} />
          <Swiper
            style           = {styles.wrapper}
            ref             = {(c) => { this.swiper = c; }}
            index           = {0}
            dot             = {<SwiperDot />}
            activeDot       = {<SwiperDotActive />}
            paginationStyle = {{ bottom: 30 }}
            loop            = {false}
          >
            <SectionContainer>
              <Title>Name your item</Title>
              <InputContainer>
                <FormInput
                  value                 = {this.state.productName}
                  placeholder           = "MY LEICA CAMERA"
                  placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                  onChangeText          = {productName => this.setState({ productName })}
                  containerStyle        = {styles.inputContainer}
                  inputStyle            = {styles.input}
                  underlineColorAndroid = "#fff"
                  returnKeyType         = "next"
                  autoCapitalize        = "characters"
                  autoCorrect           = {false}
                  onSubmitEditing       = {() => this.swiper.scrollBy(1)}
                />
                {/* {this.state.titleErr ?
                  <WarningIcon>
                    <Ionicons name="md-information-circle" size={32} color="#F87676" />
                  </WarningIcon> : null} */}
                <FormValidationMessage labelStyle={styles.inputError}>{this.state.titleErr}</FormValidationMessage>
              </InputContainer>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer>

            <SectionContainer>
              <Title>Now take a picture or upload{'\n'}your <Bold>Proof of purchase</Bold></Title>
              <TouchableOpacity onPress= {() => this._pickImage('receiptImage')}>
                <UploadImage>
                  {this.state.receiptImage ?
                    <Image
                      source={{ uri: this.state.receiptImage }}
                      style={{ width: 130, height: 130, resizeMode: Image.resizeMode.cover }}
                      onPress= {() => this._pickImage('receiptImage')}
                    /> :
                    <Image
                      source = {require('./../../../assets/icons/default/blue/Receipt.png')}
                      style  = {{
                        alignSelf: 'center',
                        width: 50,
                        height: 50,
                        resizeMode: 'contain',
                      }}
                      onPress= {() => this._pickImage('receiptImage')}
                    />
                  }
                </UploadImage>
              </TouchableOpacity>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer>

            <SectionContainer>
              <Title>Now take a picture or upload{'\n'}your <Bold>Warranty Card</Bold></Title>
              <TouchableOpacity onPress= {() => this._pickImage('warrantyImage')}>
                <UploadImage>
                  {this.state.warrantyImage ?
                    <Image
                      source={{ uri: this.state.warrantyImage }}
                      style={{ width: 130, height: 130, resizeMode: Image.resizeMode.cover }}
                      onPress= {() => this._pickImage('warrantyImage')}
                    /> :
                    <Image
                      source = {require('./../../../assets/icons/default/blue/Warranty-card.png')}
                      style  = {{
                        alignSelf: 'center',
                        width: 50,
                        height: 50,
                        resizeMode: 'contain',
                      }}
                      onPress= {() => this._pickImage('warrantyImage')}
                    />
                  }
                </UploadImage>
              </TouchableOpacity>
              <NextContainer>
                <Button
                  onPress   = {() => this._submit()}
                  rightIcon = {{
                    name : 'arrow-right',
                    type : 'feather',
                    size : 20,
                    style: styles.buttonIcon,
                  }}
                  buttonStyle = {[{ backgroundColor: 'transparent', width: '100%' }]}
                  textStyle   = {styles.buttonText}
                  title       = "SUBMIT"
                  color       = "#fff"
                />
              </NextContainer>
            </SectionContainer>
          </Swiper>
          {/* <Title>Upload your Receipt and we will{'\n'}verified your receipt.</Title>
          <UploadContainer>
            <TouchableOpacity onPress = {() => this._pickImage()}>
              <UploadReceipt>
                {receipt ?
                  <Image source={{ uri: receipt }} style={{ width: 150, height: 150, resizeMode: Image.resizeMode.cover }} />: 
                  <Image
                    source = {{ uri: 'https://via.placeholder.com/80x80'}}
                    style  = {{alignSelf: 'center', width: 80, height: 80}}
                  />
                }
              </UploadReceipt>
            </TouchableOpacity>
            <TouchableOpacity onPress = {() => this._resetUpload()}>
              <ResetText>
                Upload again
              </ResetText>
            </TouchableOpacity>
          </UploadContainer>
          <ConfirmContainer>
            <Button
              onPress     = {() => this._submitReceipt()}
              buttonStyle = {[{backgroundColor: '#fff'}, styles.button]}
              textStyle   = {styles.buttonText}
              title       = 'DONE'
              color       = "#234E7F"
            />
          </ConfirmContainer> */}
        </ContentContainer>
      </Container>
    );
  }
}

export default Screen;