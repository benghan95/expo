import React, { Component } from 'react';
import { Alert, Platform, View, StyleSheet, ScrollView, TextInput, AsyncStorage } from 'react-native';
import Expo, { AppLoading, Asset } from 'expo';
import { Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { Colors } from 'constants/styles';
import { Ionicons } from '@expo/vector-icons';
import styled from 'styled-components';
import Header from 'components/common/CustomHeader';
import Loader from 'components/common/Loader';
import CloseButton from 'components/common/CloseButton';
import moment from 'moment';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
  position       : relative;
`;

const Title = styled.Text`
  color     : #fff;
  fontFamily: 'gotham-bold';
  fontSize  : 48;
  textAlign : center;
  marginTop : 15;
`;

const TitleContainer = styled.View`
  backgroundColor: #0E6D97;
  marginBottom   : 25;
`;

const Subtitle = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-bold';
  fontSize    : 12;
  lineHeight  : 16;
  marginBottom: 25;
  textAlign   : center;
`;

const TermsText = styled.Text`
  fontFamily: 'gotham-book';
  fontSize  : 14;
  lineHeight: 18;
  textAlign : center;
`;

const LinkText = styled.Text`
  fontFamily   : 'gotham-bold';
  fontSize     : 14;
  lineHeight   : 18;
  marginTop    : 1;
  paddingBottom: 0;
  textAlign    : center;
`;

const Link = styled.TouchableOpacity`
  borderBottomWidth: 1;
  borderColor      : #979797;
  paddingBottom    : 0;
`;

const TermsContainer = styled.View`
  flexDirection    : row;
  flexWrap         : wrap;
  marginTop        : 0;
  marginBottom     : 30;
  justifyContent   : center;
  paddingHorizontal: 15;
`;

const FormContainer = styled.View`
  marginBottom     : 10;
  alignItems       : center;
  justifyContent   : center;
  paddingHorizontal: 15;
`;

const InputContainer = styled.View`
  marginBottom: 10;
  width       : 100%;
  position    : relative;
`;

const WarningIcon = styled.View`
  backgroundColor: #fff;
  position       : absolute;
  right          : 25;
  bottom         : 32;
`;

const OverlayTitle = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-bold';
  fontSize    : 14;
  marginBottom: 35;
  lineHeight  : 18;
  textAlign   : center;
`;

const OverlayWarning = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-bold';
  fontSize    : 13;
  marginBottom: 35;
  lineHeight  : 18;
  textAlign   : center;
`;

const Timer = styled.Text`
  color       : ${Colors.Default.Red};
  fontFamily  : 'gotham-bold';
  fontSize    : 13;
  marginBottom: 35;
  lineHeight  : 18;
  textAlign   : center;
`;

const CodeFormContainer = styled.View`
  flexDirection: row;
  marginBottom : 15;
`;

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: '#139AD6',
    marginBottom   : 35,
    paddingTop     : 20,
    paddingBottom  : 20,
  },
  buttonIcon: {
    marginTop  : -7,
    marginRight: 0,
  },
  buttonText: {
    color     : '#ffffff',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  },
  input: {
    color     : '#000',
    fontFamily: 'gotham-bold',
    fontSize  : 14,
    textAlign : 'center',
    width     : '100%',
    ...Platform.select({
      android: {
        paddingBottom: 15,
      },
    }),
  },
  inputContainer: {
    marginBottom     : 5,
    paddingVertical  : 5,
    paddingHorizontal: 30,
  },
  inputError: {
    fontSize  : 12,
    textAlign : 'center',
    fontFamily: 'gotham-book',
  },
  overlayContainer: {
    justifyContent   : 'center',
    alignItems       : 'center',
    flex             : 1,
    position         : 'absolute',
    paddingHorizontal: 15,
    left             : 0,
    top              : 0,
    backgroundColor  : 'rgba(0, 0, 0, 0.8)',
    height           : '100%',
    width            : '100%',
    zIndex: 999,
  },
  editButtonIcon: {
    marginTop: -5,
  },
  editButtonText: {
    color     : '#fff',
    fontSize  : 14,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  },
  verifyButtonContainer: {
    backgroundColor  : '#139AD6',
    marginBottom     : 25,
    paddingTop       : 12,
    paddingBottom    : 12,
    paddingHorizontal: 30,
    // width          : '100%',
  },
  verifyButtonText: {
    color     : '#ffffff',
    fontSize  : 16,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  },
  codeInput: {
    backgroundColor  : '#fff',
    borderBottomColor: '#fff',
    marginHorizontal : 5,
    paddingHorizontal: 0,
    width            : 40,
    paddingTop       : 15,
    paddingBottom    : 12,
    fontFamily       : 'gotham-bold',
    fontSize         : 32,
    lineHeight       : 36,
    textAlign        : 'center',
  },
});

class WelcomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id              : null,
      method          : null,
      token           : null,
      jwtToken        : null,
      isReady         : false,
      mobile          : null,
      name            : null,
      email           : null,
      mobileErr       : null,
      nameErr         : null,
      emailErr        : null,
      verificationCode: null,
      toVerify        : false,
      isReady         : true,
      loading         : false,
      timer           : 300,
    }
  }

  componentDidMount = () => {
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params.method) {
        const { method } = this.props.navigation.state.params;
        if (method != 'mobile') {
          if (this.props.navigation.state.params.data) {
            const { data } = this.props.navigation.state.params;

            this.setState({
              id   : data.id,
              name : data.name,
              email: data.email,
              token: data.token,
            });
          }
        }
      }
    }
  };

  _enterMobile = (mobile) => {
    if (!mobile.startsWith('01') && mobile && mobile.length >= 2) {
      this.setState({
        mobileErr: 'Mobile number must start with 01X.',
      });
    } else {
      this.setState({
        mobileErr: null,
      });
    }
    this.setState({ mobile });
  }

  _verifyAccount = async () => {
    let nameError   = false;
    let emailError  = false;
    let mobileError = false;
    if (!this.state.name) {
      nameError = true;
      this.setState({
        nameErr: 'Please enter your name.',
      });
    } else {
      this.setState({
        nameErr: null,
      });
    }

    if (this.state.email) {
      if (!this._validateEmail(this.state.email)) {
        emailError = true;
        this.setState({
          emailErr: 'Please enter a valid email address.',
        });
      } else {
        this.setState({
          emailErr: null,
        });
      }
    } else {
      emailError = true;
      this.setState({
        emailErr: 'Please enter your email address.',
      });
    }

    if (!this.state.mobile) {
      mobileError = true;
      this.setState({
        mobileErr: 'Please enter your mobile number.',
      });
    } else {
      this.setState({
        mobileErr: null,
      });
    }

    if (!this.state.mobile.startsWith('01')) {
      mobileError = true;
      this.setState({
        mobileErr: 'Mobile number must start with 01X.',
      });
    } else {
      this.setState({
        mobileErr: null,
      });
    }

    if (!nameError && !mobileError && !emailError) {
      this.setState({ loading: true });
      const param = {};

      param.email    = this.state.email;
      param.mobile   = this.state.mobile.replace(/\s+/g, '');
      param.fullName = this.state.name;
      if (this.props.navigation.state.params.method) {
        if (this.props.navigation.state.params.method === 'facebook') {
          param.facebookId    = this.props.navigation.state.params.data.id;
          param.facebookToken = this.props.navigation.state.params.data.token;
        } else if (this.props.navigation.state.params.method === 'google') {
          param.googleId    = this.props.navigation.state.params.data.id;
          param.googleToken = this.props.navigation.state.params.data.token;
        }
      }
      const token = await AsyncStorage.getItem('Kepp.jwtToken');
      await fetch('https://www.kepp.link/user/login', {
        method : 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(param),
      })
        .then(response => response.json())
        .then((responseJson) => {
          this.setState({ loading: false });
          if (responseJson.statusResp) {
            if (parseInt(responseJson.statusResp.responseCode, 10) >= 100000) {
              this.setState({
                jwtToken: responseJson.statusResp.jwtToken,
              });
              this._sendVerificationCode();
            } else {
              Alert.alert(responseJson.statusResp.responseDesc);
            }
          } else {
            Alert.alert('Error occured! Please try again later.');
          }
        })
        .catch((err) => {
          console.log(err);
          this.setState({ loading: false });
          Alert.alert(JSON.stringify(err));
        });
    }
  }

  _tick = () => {
    if (this.state.timer > 0) {
      this.setState(prevState => ({
        timer: prevState.timer - 1,
      }));
    } else {
      clearInterval(this.timer);
      Alert.alert('Expired', 'Verification code that has been sent to you is expired, please try again later.', [
        {
          text: 'Back',
          onPress: async () => {
            await this.setState({
              timer: 300,
              toVerify: false,
            });
          },
        },
      ]);
    }
  }

  _sendVerificationCode = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem('Kepp.jwtToken');
    await fetch('https://www.kepp.link/user/verify', {
      method : 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email : this.state.email,
        mobile: `+6${this.state.mobile.replace(/\s+/g, '')}`,
      }),
    })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({ loading: false });
        if (responseJson.statusResp) {
          if (parseInt(responseJson.statusResp.responseCode, 10) >= 100000) {
            this.setState({ toVerify: true });
            this.setState({ verificationCode: responseJson.verificationCode });
            this.timer = setInterval(() => this._tick(), 1000);
          } else {
            Alert.alert(responseJson.statusResp.responseDesc);
          }
        } else {
          Alert.alert('Error occured! Please try again later.');
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  _validateEmail = (mail) => {
    const pattern = /^[\w-.]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    return mail.match(pattern);
  }

  _verifyCode = async () => {
    const code   = this.state.verificationCode;
    const digits = code.split('');

    if (this.state.firstCode === digits[0]
     && this.state.secondCode === digits[1]
     && this.state.thirdCode === digits[2]
     && this.state.forthCode === digits[3]) {
      try {
        await AsyncStorage.setItem('Kepp.jwtToken', this.state.jwtToken);
        clearInterval(this.timer);
        this.setState({
          timer: 300,
        });
        this.props.navigation.navigate('Home');
      } catch (err) {
        Alert.alert(JSON.stringify(err));
      }
    } else {
      Alert.alert('Verification code is incorrect!');
    }
  }

  // _mobileOnFocus = () => {
  //   this.setState({
  //     mobile: '+60 '
  //   })
  // }

  _focusNextField = (nextField) => {
    this[nextField].focus();
  };

  _onKeyPress = (action, stateName, prevField) => {
    if (action.nativeEvent.key === 'Backspace') {
      if (prevField) {
        this[prevField].focus();
      }
    }
  }

  _changeDigit = (text, stateName, nextField) => {
    this.setState({
      [stateName]: text,
    });
    if (text && text.length === 1) {
      this[nextField].focus();
    }
  }

  _cancelVerify = () => {
    this.setState({ toVerify: false });
    clearInterval(this.timer);
    this.setState({
      timer: 300,
    });
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading} />
        {this.state.toVerify ?
          <View style={styles.overlayContainer}>
            <CloseButton style={{ top: 10 }} onPress={() => this._cancelVerify()} />
            <OverlayTitle>A 4-digit Verification code will be sent to{'\n'}+6{this.state.mobile} via SMS or a phone call.{'\n'}Please enter the code below</OverlayTitle>
            <OverlayWarning>The code will valid for <Timer>{ moment.utc(this.state.timer * 1000).format('mm:ss') }</Timer> minutes</OverlayWarning>
            <CodeFormContainer>
              <TextInput
                {...this.props}
                enablesReturnKeyAutomatically
                underlineColorAndroid = "transparent"
                style                 = {styles.codeInput}
                ref                   = {(c) => { this.first_digit = c; }}
                keyboardType          = "numeric"
                returnKeyType         = "next"
                clearTextOnFocus      = {false}
                autoCorrect           = {false}
                onKeyPress            = {e => this._onKeyPress(e, 'firstCode', null)}
                onChangeText          = {firstCode => this._changeDigit(firstCode, 'firstCode', 'second_digit')}
                value                 = {this.state.firstCode}
                placeholder           = "0"
                maxLength             = {1}
                onSubmitEditing       = {() => this._focusNextField('second_digit')}
              />
              <TextInput
                {...this.props}
                enablesReturnKeyAutomatically
                underlineColorAndroid = "transparent"
                style                 = {styles.codeInput}
                ref                   = {(c) => { this.second_digit = c; }}
                keyboardType          = "numeric"
                returnKeyType         = "next"
                clearTextOnFocus      = {false}
                autoCorrect           = {false}
                onKeyPress            = {e => this._onKeyPress(e, 'secondCode', 'first_digit')}
                onChangeText          = {secondCode => this._changeDigit(secondCode, 'secondCode', 'third_digit')}
                value                 = {this.state.secondCode}
                placeholder           = "0"
                maxLength             = {1}
                onSubmitEditing       = {() => this._focusNextField('third_digit')}
              />
              <TextInput
                {...this.props}
                enablesReturnKeyAutomatically
                underlineColorAndroid = "transparent"
                style                 = {styles.codeInput}
                ref                   = {(c) => { this.third_digit = c; }}
                keyboardType          = "numeric"
                returnKeyType         = "next"
                clearTextOnFocus      = {false}
                autoCorrect           = {false}
                onKeyPress            = {e => this._onKeyPress(e, 'thirdCode', 'second_digit')}
                onChangeText          = {thirdCode => this._changeDigit(thirdCode, 'thirdCode', 'forth_digit')}
                value                 = {this.state.thirdCode}
                placeholder           = "0"
                maxLength             = {1}
                onSubmitEditing       = {() => this._focusNextField('forth_digit')}
              />
              <TextInput
                {...this.props}
                enablesReturnKeyAutomatically
                underlineColorAndroid = "transparent"
                style                 = {styles.codeInput}
                ref                   = {(c) => { this.forth_digit = c; }}
                keyboardType          = "numeric"
                returnKeyType         = "done"
                clearTextOnFocus      = {false}
                autoCorrect           = {false}
                onKeyPress            = {e => this._onKeyPress(e, 'forthCode', 'third_digit')}
                onChangeText          = {forthCode => this.setState({ forthCode })}
                value                 = {this.state.forthCode}
                placeholder           = "0"
                maxLength             = {1}
              />
            </CodeFormContainer>
            <Button
              onPress = {() => this._verifyCode()}
              // onPress     = {() => this.props.navigation.navigate('Login')}
              buttonStyle = {styles.verifyButtonContainer}
              textStyle   = {styles.verifyButtonText}
              title       = "VERIFY"
              color       = "#841584"
            />
            <Button
              icon    = {{ name: 'edit-2', type: 'feather', size: 16, style: styles.editButtonIcon }}
              onPress = {() => this._cancelVerify()}
              // onPress     = {() => this.props.navigation.navigate('Login')}
              buttonStyle = {{backgroundColor: 'transparent'}}
              textStyle   = {styles.editButtonText}
              title       = "EDIT MOBILE NUMBER"
              color       = "#841584"
            />
          </View>
        : null}
        <ScrollView>
          <Header type="backOnly" bgColor="#0E6D97" navigation={this.props.navigation} />
          <TitleContainer>
            <Title>WELCOME!</Title>
            <Subtitle>Before proceed, we need to verify your{'\n'} mobile number.</Subtitle>
          </TitleContainer>
          <FormContainer>
            <InputContainer>
              <FormInput
                value          = {this.state.mobile}
                ref            = {(c) => { this.mobileInput = c; }}
                placeholder    = "MOBILE NUMBER"
                onChangeText   = {mobile => this._enterMobile(mobile)}
                containerStyle = {styles.inputContainer}
                inputStyle     = {styles.input}
                keyboardType   = "phone-pad"
                returnKeyType  = "next"
                autoCapitalize = "none"
                autoCorrect    = {false}
                onSubmitEditing = {() => this.nameInput.focus()}
                // onFocus        = {() => this._mobileOnFocus()}
              />
              {this.state.mobileErr ?
                <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
              <FormValidationMessage labelStyle={styles.inputError}>{this.state.mobileErr}</FormValidationMessage>
            </InputContainer>
            <InputContainer>
              <FormInput
                value           = {this.state.name}
                ref             = {(c) => { this.nameInput = c; }}
                placeholder     = "FULL NAME"
                onChangeText    = {name => this.setState({ name })}
                containerStyle  = {styles.inputContainer}
                inputStyle      = {styles.input}
                returnKeyType   = "next"
                autoCapitalize  = "words"
                autoCorrect     = {false}
                onSubmitEditing = {() => this.emailInput.focus()}
              />
              {this.state.nameErr ?
                <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
              <FormValidationMessage labelStyle={styles.inputError}>{this.state.nameErr}</FormValidationMessage>
            </InputContainer>
            <InputContainer>
              <FormInput
                value          = {this.state.email}
                ref            = {(c) => { this.emailInput = c; }}
                placeholder    = "EMAIL ADDRESS"
                onChangeText   = {email => this.setState({ email })}
                containerStyle = {styles.inputContainer}
                inputStyle     = {styles.input}
                returnKeyType  = "done"
                autoCapitalize = "none"
                autoCorrect    = {false}
                // onSubmitEditing = {() => this.verifyButton.focus()}
              />
              {this.state.emailErr ?
                <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
              <FormValidationMessage labelStyle={styles.inputError}>{this.state.emailErr}</FormValidationMessage>
            </InputContainer>
          </FormContainer>
          <TermsContainer>
            <View>
              <TermsText>By signing in, I have read and agreed to the </TermsText>
            </View>
            <Link>
              <LinkText>TERMS AND CONDITIONS</LinkText>
            </Link>
            <View>
              <TermsText> and </TermsText>
            </View>
            <Link>
              <LinkText>PRIVACY POLICY</LinkText>
            </Link>
          </TermsContainer>
          <Button
            iconRight   = {{
              name: 'arrow-right',
              type: 'feather',
              size: 26,
              style: styles.buttonIcon,
            }}
            onPress     = {() => this._verifyAccount()}
            buttonStyle = {styles.buttonContainer}
            textStyle   = {styles.buttonText}
            title       = "VERIFY"
            color       = "#841584"
          />
        </ScrollView>
      </Container>
    );
  }
}

export default WelcomeScreen;