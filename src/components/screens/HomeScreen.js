import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image, ScrollView, Dimensions, Animated, BackHandler, AsyncStorage, RefreshControl } from 'react-native';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { Button, Icon } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import ProductCard from 'components/common/ProductCard';
import ProductProfile from 'components/common/ProductProfile';
import Header from 'components/common/CustomHeader';
import Loader from 'components/common/Loader';
import { NavigationActions } from 'react-navigation';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const ProductsListContainer = styled.View`
  alignItems: center;
  flex      : 1;
`;

const EmptyContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
`;

const AddMoreContainer = styled.View`
  marginTop     : 30;
  marginBottom  : 50;
  alignItems    : center;
  justifyContent: center;
`;

const CTATitle = styled.Text`
  color            : #717171;
  fontFamily       : 'gotham-book';
  fontSize         : 14;
  marginBottom     : 25;
  textAlign        : center;
  paddingHorizontal: 20;
`;

// const styles = StyleSheet.create({
// })

// cacheImages = (images) => {
//   return images.map(image => {
//     if (typeof image === 'string') {
//       return Image.prefetch(image);
//     } else {
//       return Asset.fromModule(image).downloadAsync();
//     }
//   });
// }

// cacheFonts = (fonts) => {
//   return fonts.map(font => Font.loadAsync(font));
// }

class Screen extends Component {
  // async _loadAssetsAsync() {
  //   const imageAssets = cacheImages([
  //     require('./assets/images/circle.jpg'),
  //   ]);

  //   const fontAssets = cacheFonts([FontAwesome.font]);

  //   await Promise.all([...fontAssets]);
  //   await Promise.all([...imageAssets, ...fontAssets]);
  // }

  constructor(props) {
    super(props);
    this.state = {
      isReady    : true,
      refreshing : false,
      loading    : false,
      toggled    : false,
      items      : [],
      isHome     : true,
      productData: {
        title         : null,
        category      : null,
        purchaseDate  : null,
        warrantyPeriod: null,
        image         : null,
      },
    };

    this.currentRouteName = 'Main';
  }

  componentWillMount() {
  }

  componentDidMount() {
    console.log(this.props.navigation.state.routeName);
    BackHandler.addEventListener('hardwareBackPress', () => {
      const { navigation } = this.props;

      if ('state' in navigation) {
        if ('routeName' in navigation.state) {
          console.log(this.props.navigation.state.routeName);
          if (this.props.navigation.state.routeName === 'Home') {
            BackHandler.exitApp();
          }
        }
      }
    });

    this._getItems();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  _getItems = async () => {
    try {
      const token = await AsyncStorage.getItem('Kepp.jwtToken');
      this.setState({ loading: true });
      if (token) {
        fetch('https://www.kepp.link/secure/getitems', {
          method : 'POST',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
          .then(response => response.json())
          .then((responseJson) => {
            this.setState({ loading: false });
            if (responseJson.statusResp) {
              const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
              if (responseCode >= 100000 && responseCode < 200000) {
                this.setState({
                  items: responseJson.items,
                });
              }
            }
          })
          .catch((err) => {
            this.setState({ loading: false });
            Alert.alert(JSON.stringify(err));
          });
      } else {
        this.setState({ loading: false });
        this.props.navigation.navigate('Login');
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err));
    }
  }

  _onRefresh = () => {
    this._getItems();
  }

  _toggleDisplay = (data) => {
    this.setState({
      toggled    : true,
      selectedId : data.itemId,
      productData: data,
    });
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading = {this.state.loading} />
        <Header
          type                = "default"
          bgColor             = {this.state.toggled ? Colors.Default.Cyan : "#234E7F"}
          navigation          = {this.props.navigation}
          leftComponent       = {this.state.toggled ? true : null}
          uniqueLeftComponent = {
            <View>
              <TouchableOpacity onPress={() => this.setState({toggled: false})}>
                <View style={{ marginLeft: 10, paddingVertical: 17, paddingHorizontal: 15 }}>
                  <Image
                    source = {require('./../../../assets/icons/default/white/Close.png')}
                    style  = {{ width: 20, height: 20, resizeMode: 'contain', }}
                  />
                </View>
              </TouchableOpacity>
            </View>
          }
        />
        {this.state.toggled ?
          <ProductProfile data={this.state.productData} navigation={this.props.navigation} /> : null
        }
        {this.state.items.length > 0 ?
          <ProductsListContainer>
            <ScrollView
              contentContainerStyle = {{ paddingTop: 20 }}
              refreshControl        = {
                <RefreshControl
                  refreshing = {this.state.refreshing}
                  onRefresh  = {this._onRefresh.bind(this)}
                />
              }
            >
              {
                this.state.items.map((prop) => {
                  return (
                    <ProductCard
                      key            = {prop.itemId}
                      itemId         = {prop.itemId}
                      title          = {prop.itemShortName}
                      purchaseDate   = {prop.purchaseDate}
                      warrantyDays   = {prop.warrantyDays}
                      protectionDays = {prop.protectionDays}
                      productImage   = {prop.productImage}
                      categoryId     = {prop.categoryId}
                      onPress        = {this._toggleDisplay.bind(this)}
                      type           = {prop.type}
                      status         = {prop.status}
                      active         = {this.state.selectedId === prop.itemId ? true : false}
                    />
                  );
                })
              }
              <AddMoreContainer>
                <Icon
                  name           = "plus"
                  type           = "font-awesome"
                  color          = "#868686"
                  component      = {TouchableOpacity}
                  containerStyle = {{
                    borderStyle : 'dotted',
                    borderRadius: 100,
                    borderWidth : 2,
                    borderColor : '#8A8A8A',
                    marginBottom: 11,
                  }}
                  iconStyle = {{paddingVertical: 23, paddingHorizontal: 25}}
                  onPress   = { () => this.props.navigation.navigate('UploadMethod') }
                  size      = {18}
                />
                <CTATitle>Add More</CTATitle>
                {/* <CTATitle>Hi, adding your warranty is easier{'\n'}than even before, try me!</CTATitle> */}
              </AddMoreContainer>
            </ScrollView>
          </ProductsListContainer> :
          <EmptyContainer>
            <ScrollView
              contentContainerStyle = {{flex: 1, alignItems: 'center', justifyContent: 'center' }}
              style                 = {{width: '100%'}}
              refreshControl        = {
                <RefreshControl
                  refreshing = {this.state.refreshing}
                  onRefresh  = {this._onRefresh.bind(this)}
                />
              }>
              <Icon
                name           = "plus"
                type           = "font-awesome"
                color          = "#868686"
                component      = {TouchableOpacity}
                containerStyle = {{
                  borderStyle : 'dotted',
                  borderRadius: 100,
                  borderWidth : 2,
                  borderColor : '#8A8A8A',
                  marginBottom: 18,
                }}
                iconStyle = {{ paddingVertical: 23, paddingHorizontal: 25 }}
                onPress   = {() => this.props.navigation.navigate('UploadMethod')}
                size      = {18}
              />
              <CTATitle>Hi, adding your warranty is easier{'\n'}than even before, try me!</CTATitle>
              {/* <CTATitle>Ops! it seem haven’t store any{"\n"}warranty yet, upload now</CTATitle> */}
            </ScrollView>
          </EmptyContainer>
        }
      </Container>
    );
  }
}

export default Screen;