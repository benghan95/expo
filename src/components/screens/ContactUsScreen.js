import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image, AsyncStorage, Dimensions, TextInput } from 'react-native';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { Button, Icon } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import Header from 'components/common/CustomHeader';
import QRCode from 'react-native-qrcode';
import Loader from 'components/common/Loader';
import CloseButton from 'components/common/CloseButton';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const LogoContainer = styled.View`
  alignItems: center;
  marginBottom: 20;
`;

const ContentContainer = styled.View`
  flex             : 1;
  paddingTop       : 60;
  paddingHorizontal: 10;
  paddingBottom    : 35;
  ${'' /* alignItems    : center; */}
  ${'' /* justifyContent: center; */}
`;

const Title = styled.Text`
  color            : #333;
  fontFamily       : 'gotham-book';
  fontSize         : 24;
  textAlign        : center;
  paddingHorizontal: 20;
  marginBottom     : 5;
`;

const Subtitle = styled.Text`
  color            : #333;
  fontFamily       : 'gotham-book';
  fontSize         : 14;
  textAlign        : center;
  paddingHorizontal: 20;
  marginBottom     : 35;
`;

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: Colors.Default.Cyan,
    paddingTop     : 20,
    paddingBottom  : 20,
  },
  buttonText: {
    color     : '#ffffff',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  },
  buttonIcon: {
    ...Platform.select({
      ios: {
        marginRight: 15,
        marginTop  : -2,
      },
      android: {
        marginRight: 15,
        marginTop  : 0,
      },
    }),
  },
  button: {
    paddingTop   : 20,
    paddingBottom: 18,
    marginBottom : 8,
  },
});

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      loading: false,
      message: '',
    };
  }

  _sendMessage = async () => {
    this.setState({ loading: true });

    if (!this.state.message) {
      Alert.alert('Please enter your message.');
      this.setState({ loading: false });
      return false;
    }

    const token = await AsyncStorage.getItem('Kepp.jwtToken');
    const data = {
      message: this.state.message,
    };

    fetch('https://www.kepp.link/secure/contactus', {
      method : 'POST',
      headers: {
        Authorization : `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({ loading: false });
        if (responseJson.statusResp) {
          const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
          if (responseCode >= 100000 && responseCode < 200000) {
            Alert.alert('Message has been sent, we will reply you in a while.', '', [
              {
                text: 'OK',
                onPress: () => { this.props.navigation.pop(); },
              },
            ]);
          } else {
            Alert.alert(responseJson.statusResp.responseDesc);
          }
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }


  render() {
    return (
      <Container>
        <Loader loading = {this.state.loading} />
        <ContentContainer>
          <CloseButton blue style={{top: 10, right: 10,}} onPress={() => this.props.navigation.pop()} />
          <LogoContainer>
            <Image
              source={require('./../../../assets/images/logo-contactUs.png')}
              style={{width: 45, height: 45, resizeMode: 'contain'}}
            />
          </LogoContainer>
          <Title>CONTACT US</Title>
          <Subtitle>Feel free to drop us a line below!</Subtitle>
          <TextInput
            multiline
            ref           = {(c) => { this.messageInput = c; }}
            placeholder   = "Type your message here..."
            onChangeText  = {message => this.setState({ message })}
            style         = {{
              fontSize: 16,
              fontFamily: 'gotham-book',
              borderWidth: 1,
              borderColor: '#666',
              paddingHorizontal: 15,
              paddingTop: 15,
              paddingBottom: 15,
              marginBottom: 20,
              marginHorizontal: 15,
              shadowColor  : '#000000',
              shadowOpacity: 0.3,
              shadowOffset : {
                width : 0,
                height: 1,
              },
              shadowRadius: 2,
              elevation   : 1,
              height: (height - 350),
            }}
          />
          <Button
            onPress        = {() => this._sendMessage()}
            buttonStyle    = {styles.buttonContainer}
            textStyle      = {styles.buttonText}
            title          = "SEND"
            backgroudColor = {Colors.Default.Cyan}
          />
        </ContentContainer>
      </Container>
    );
  }
}

export default Screen;
