import React, { Component } from 'react';
import { Platform, TouchableOpacity, AsyncStorage, Alert, View, StyleSheet, Image } from 'react-native';
import { Button } from 'react-native-elements';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { FontAwesome, Feather } from '@expo/vector-icons';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import Loader from 'components/common/Loader';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #fff;
`;

const LogoView = styled.View`
  flex          : 4;
  alignItems    : center;
  ${'' /* justifyContent: center; */}
  justifyContent: flex-end;
  paddingBottom: 30;
`;

const ActionView = styled.View`
  flex          : 2;
  alignItems    : center;
  justifyContent: flex-start;
`;

const styles = StyleSheet.create({
  getStartedIcon: {
    color: '#139AD6',
    ...Platform.select({
      ios: {
        marginTop  : -7,
        marginRight: 0,
      },
      android: {
        marginTop  : 0,
        marginRight: 0,
      },
    }),
  },
  getStartedText: {
    color     : '#139AD6',
    fontSize  : 24,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  }
})

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}


export default class LandingScreen extends Component {
  state = {
    isReady: true,
    loading: false,
  };

  async _loadAssetsAsync() {
    // const imageAssets = cacheImages([
    //   require('./assets/images/circle.jpg'),
    // ]);

    const fontAssets = cacheFonts([FontAwesome.font, Feather.font]);

    await Promise.all([...fontAssets]);
    // await Promise.all([...imageAssets, ...fontAssets]);
  }

  _onStart = async () => {
    this.setState({ loading: true });
    try {
      const token = await AsyncStorage.getItem('Kepp.jwtToken');
      if (token) {
        fetch('https://www.kepp.link/secure/userdetail', {
          method : 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
          .then(response => response.json())
          .then(async (responseJson) => {
            this.setState({ loading: false });
            if (responseJson.statusResp) {
              const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
              console.log(responseJson);
              if (responseCode >= 100000 && responseCode < 200000) {
                await AsyncStorage.setItem('Kepp.name', responseJson.firstName);
                this.props.navigation.navigate('Home');
              } else {
                await AsyncStorage.removeItem('Kepp.jwtToken');
                this.props.navigation.navigate('Login');
              }
            } else {
              await AsyncStorage.removeItem('Kepp.jwtToken');
              this.props.navigation.navigate('Login');
            }
          })
          .catch(async () => {
            await AsyncStorage.removeItem('Kepp.jwtToken');
            this.setState({ loading: false });
            this.props.navigation.navigate('Login');
            // Alert.alert(JSON.stringify(err));
          });
      } else {
        this.setState({ loading: false });
        this.props.navigation.navigate('Login');
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err));
    }
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading}/>
        <LogoView>
          <Image
            source     = {require('./../../../assets/images/kepp-intro-animation.gif')}
            style      = {{width: 200, height: 335, resizeMode: 'contain' }}
          />
        </LogoView>
        <ActionView>
          <Button
            iconRight   = {{ name: 'arrow-right', type: 'feather', size: 32, style: styles.getStartedIcon }}
            onPress     = {() => this._onStart()}
            buttonStyle = {{backgroundColor: 'transparent'}}
            textStyle   = {styles.getStartedText}
            component   = {TouchableOpacity}
            title       = "GET STARTED"
            color       = "#841584"
          />
        </ActionView>
      </Container>
    );
  }
}