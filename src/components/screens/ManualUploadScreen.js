import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, TouchableHighlight, Text, StyleSheet, View, Image, Dimensions, AsyncStorage } from 'react-native';
import Expo, { Font, AppLoading, Asset, ImagePicker, ImageManipulator, Permissions } from 'expo';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { Button, Icon, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import Header from 'components/common/CustomHeader';
import Loader from 'components/common/Loader';
import NextButton from 'components/common/NextButton';
import Swiper from 'react-native-swiper';
// import Slider from 'react-native-slider';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
// import Carousel from 'react-native-snap-carousel';
import PickerSelect from 'react-native-picker-select';
import { categories } from 'data/categories';
import { months, years } from 'data/constants';
import { proofEntries, warrantyEntries } from 'data/entries';
import CloseButton from 'components/common/CloseButton';
import Modal from 'react-native-modal';

const { width, height } = Dimensions.get('window');

const Container = styled.View`
  flex           : 1;
  backgroundColor: #234E7F;
`;

const ContentContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
`;

const SectionContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
  marginTop     : -70;
`;

const SwiperDot = styled.View`
  backgroundColor: #fff;
  width          : 5;
  height         : 5;
  borderRadius   : 7;
  marginLeft     : 6;
  marginRight    : 6;
`;

const SwiperDotActive = styled.View`
  backgroundColor: #13A1FF;
  width          : 10;
  height         : 10;
  borderRadius   : 7;
  marginLeft     : 6;
  marginRight    : 6;
`;

const ModalOption = styled.TouchableHighlight`
  paddingHorizontal: 15;
  paddingVertical: 15;
`;

const ModalOptionText = styled.Text`
  fontSize    : 18;
  fontFamily  : 'gotham-book';
`;

const Title = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-book';
  fontSize    : 18;
  marginBottom: 35;
  textAlign   : center;
  opacity     : 0.6;
`;

const Bold = styled.Text`
  fontFamily: 'gotham-bold';
`;

const InputContainer = styled.View`
  marginBottom: 20;
  width       : 100%;
  position    : relative;
`;

const NextContainer = styled.View`
  flexDirection: row;
`;

const NextText = styled.Text`
  color: #fff;
`;

const UploadImage = styled.View`
  alignItems     : center;
  overflow       : hidden;
  justifyContent : center;
  backgroundColor: #D8D8D8;
  borderRadius   : 130;
  height         : 130;
  width          : 130;
  marginBottom   : 20;
`;

const CategoriesListContainer = styled.View`
  height      : 280;
  marginBottom: 30;
`;

const CategoryRow = styled.View`
  flexDirection: row;
  width        : 260;
`;

const CategoryCard = styled.View`
  marginHorizontal: 10;
  width           : 75;
`;

const CategoryImage = styled.View`
  borderWidth   : 1;
  borderColor   : #fff;
  borderRadius  : 15;
  justifyContent: center;
  alignItems    : center;
  width         : 75;
  height        : 75;
`;

const CategoryLabel = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-book';
  fontSize    : 12;
  textAlign   : center;
  marginTop   : 10;
  marginBottom: 20;
`;

const WarrantyContainer = styled.View`
  justifyContent   : center;
  flexDirection    : row;
  paddingHorizontal: 35;
  marginBottom     : 35;
  width            : 100%;
`;

const FormContainer = styled.View`
  height: ${height - 250};
  ${'' /* height           : 300; */}
  paddingHorizontal: 30;
  width            : 100%;
`;

const FormInputContainer = styled.View`
  position    : relative;
  marginBottom: 10;
`;

const styles = StyleSheet.create({
  formLabel: {
    color      : '#fff',
    opacity    : 0.65,
    fontSize   : 14,
    marginLeft : 0,
    marginRight: 0,
    marginTop  : 0,
  },
  input: {
    color     : '#fff',
    fontFamily: 'gotham-bold',
    fontSize  : 24,
    textAlign : 'center',
    ...Platform.select({
      android: {
        paddingBottom: 15,
      },
    }),
    width     : '100%',
  },
  inputContainer: {
    borderBottomColor: 'rgba(255, 255, 255, 0.4)',
    marginBottom     : 5,
    marginLeft       : 35,
    marginRight      : 35,
    ...Platform.select({
      marginLeft       : 0,
      marginRight      : 0,
    }),
    paddingVertical  : 5,
    paddingHorizontal: 20,
  },
  inputError: {
    fontSize  : 12,
    textAlign : 'center',
    fontFamily: 'gotham-book',
  },
  wrapper: {
    alignItems    : 'center',
    justifyContent: 'center',
  },
  buttonIcon: {
    ...Platform.select({
      ios: {
        marginLeft: 5,
        marginTop : -2,
      },
      android: {
        marginLeft: 5,
        marginTop : 0,
      },
    }),
  },
  datepicker: {
    width       : '100%',
    marginTop   : 10,
    marginBottom: 10,
  },
  dateInput: {
    alignItems       : 'center',
    paddingBottom    : 5,
    borderColor      : 'transparent',
    borderWidth      : 1,
    borderBottomColor: '#fff',
  },
  dateText:{
    fontSize: 24,
    color   : '#fff',
  },
  placeholderText: {
    fontSize: 24,
    color   : '#1d3e68',
  },
});

// const slider = StyleSheet.create({
//   track: {
//     height         : 2,
//     borderRadius   : 0,
//     backgroundColor: '#fff',
//     opacity        : 0.4,
//   },
//   thumb: {
//     width          : 30,
//     height         : 30,
//     borderRadius   : 30 / 2,
//     backgroundColor: '#fff',
//     borderColor    : '#67C7FF',
//     borderWidth    : 4,
//     shadowColor    : '#000000',
//     shadowOpacity  : 0.5,
//     shadowOffset   : {
//       width : 0,
//       height: 2,
//     },
//     shadowRadius: 4,
//   },
// });

// cacheImages = (images) => {
//   return images.map(image => {
//     if (typeof image === 'string') {
//       return Image.prefetch(image);
//     } else {
//       return Asset.fromModule(image).downloadAsync();
//     }
//   });
// }

// cacheFonts = (fonts) => {
//   return fonts.map(font => Font.loadAsync(font));
// }

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      proofEntries,
      warrantyEntries,
      duration        : 4,
      isReady         : true,
      loading         : false,
      productName     : null,
      productNameErr  : null,
      productImage    : null,
      productImageErr : null,
      productImageId  : null,
      receiptImage    : null,
      receiptImageId  : null,
      warrantyImage   : null,
      warrantyImageId : null,
      warrantyYear    : 1,
      warrantyMonth   : 0,
      warrantyDuration: 0,
      model           : null,
      purchaseDate    : null,
      purchaseLocation: null,
      purchasePrice   : null,
      serialNo        : null,
      currentSlide    : 0,
      showModal       : false,
      handlingImage   : null,
    };
  }

  componentDidMount() {
  }

  // async _loadAssetsAsync() {
  //   const imageAssets = cacheImages([
  //     require('./assets/images/circle.jpg'),
  //   ]);

  //   const fontAssets = cacheFonts([FontAwesome.font]);

  //   await Promise.all([...fontAssets]);
  //   await Promise.all([...imageAssets, ...fontAssets]);
  // }

  _nextSlide = () => {
    this._swiper.scrollBy(1);
  }

  _uploadImage = async (token, formData, type, state) => {
    await fetch(`https://www.kepp.link/secure/${type}`, {
      method : 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type' : 'multipart/form-data',
      },
      body: formData,
    })
      .then(response => response.json())
      .then((responseJson) => {
        if (responseJson.statusResp) {
          if (parseInt(responseJson.statusResp.responseCode, 10) >= 100000) {
            this.setState({
              [state]: responseJson.imageId,
            });
          }
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
        return null;
      });
  }

  _getFormData = (state) => {
    const formData = new FormData();
    const uri      = this.state[state];
    const uriArr   = uri.split('/');
    const filename = uriArr[uriArr.length - 1];
    const fileArr  = filename.split('.');
    const ext      = fileArr[fileArr.length - 1];
    formData.append('file', {
      uri,
      name: `${filename}`,
      type: `image/${ext}`,
    });

    return formData;
  }

  _calcDuration = () => {
    let duration = 0;
    duration += this.state.warrantyMonth * 30;
    duration += this.state.warrantyYear * 365;

    this.setState({ warrantyDuration: duration });
  }

  _processData = async (token) => {
    if (this.state.productImage) {
      const productImageData = this._getFormData('productImage');
      await this._uploadImage(token, productImageData, 'uploadproduct', 'productImageId');
    }

    if (this.state.receiptImage) {
      const receiptImageData = this._getFormData('receiptImage');
      await this._uploadImage(token, receiptImageData, 'uploadreceipt', 'receiptImageId');
    }
    if (this.state.warrantyImage) {
      const warrantyImageData = this._getFormData('warrantyImage');
      await this._uploadImage(token, warrantyImageData, 'uploadwarrantycard', 'warrantyImageId');
    }

    await this._calcDuration();

    return this.state;
  }

  _submit = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem('Kepp.jwtToken');

    if (!this.state.productName) {
      Alert.alert('Please enter your product title.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.productImage) {
      Alert.alert('Please upload your product image.');
      this.setState({ loading: false });
      return false;
    }

    // if (!this.state.receiptImage) {
    //   Alert.alert('Please upload your product receipt.');
    //   this.setState({ loading: false });
    //   return false;
    // }

    // if (!this.state.warrantyImage) {
    //   Alert.alert('Please upload your product receipt.');
    //   this.setState({ loading: false });
    //   return false;
    // }

    if (this.state.warrantyMonth == 0 && this.state.warrantyYear == 0) {
      Alert.alert('Please select the warranty duration.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.productModel) {
      Alert.alert('Please enter your product model.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.purchaseDate) {
      Alert.alert('Please enter your purchase date.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.shopLocation) {
      Alert.alert('Please enter the location that you purchase your product.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.productPrice) {
      Alert.alert('Please enter the price that you purchase your product.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.seriesNo) {
      Alert.alert('Please enter the serial no. that displayed on your product.');
      this.setState({ loading: false });
      return false;
    }

    const process = await this._processData(token);

    const data = {
      itemShortName    : this.state.productName,
      categoryId       : this.state.categoryId.toString(),
      productImage     : this.state.productImageId,
      receiptImage     : this.state.receiptImageId,
      warrantyCardImage: this.state.warrantyImageId,
      purchaseLocation : this.state.shopLocation,
      purchasePrice    : this.state.productPrice,
      purchaseDate     : this.state.purchaseDate,
      serialNo         : this.state.seriesNo,
      warrantyDuration : this.state.warrantyDuration,
      model            : this.state.productModel,
      // 'purchaseDate'     : this.state.purchaseDate.replace(/\-/g, ''),
    };

    fetch('https://www.kepp.link/secure/manualentry', {
      method : 'POST',
      headers: {
        Authorization : `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({ loading: false });
        if ('itemShortName' in responseJson) {
          this.props.navigation.navigate('ManualUploadSuccess');
        } else {
          Alert.alert(JSON.stringify(responseJson));
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  _onSelectCategory = (categoryId) => {
    this.setState({ categoryId });
  }

  _pickImage = async (param) => {
    this.setState({
      handlingImage: param,
      showModal: true,
    });
  };

  _takePhoto = async (param) => {
    this.setState({
      showModal: false,
    });

    let granted = false;

    const allowedCamera = await Permissions.askAsync(Permissions.CAMERA);

    if (allowedCamera.status !== 'granted') {
      Alert.alert('Warning', 'Please allow KEPP to open your Camera so that you can snap a photo to upload.');
      return;
    }

    if (Platform.OS === 'ios') {
      const allowedCameraRoll = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (allowedCameraRoll.status !== 'granted') {
        Alert.alert('Warning', 'Please allow KEPP to open your Camera Roll so that you can choose an image to upload.');
        return;
      }
    }

    granted = true;

    if (granted) {
      const result = await ImagePicker.launchCameraAsync({
        quality: 0.72,
      });

      if (!result.cancelled) {
        const manipResult = await ImageManipulator.manipulate(
          result.uri,
          [{ resize: { width: 480 } }],
          { format: 'jpeg' },
        );
        this.setState({ [param]: manipResult.uri });
      }
    } else {
      Alert.alert('Error', 'Please check your app permission and allow KEPP to open your Camera and Camera Roll to upload an image.');
    }
  }

  _chooseImage = async (param) => {
    this.setState({
      showModal: false,
    });

    let granted = false;
    if (Platform.OS === 'ios') {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status === 'granted') {
        granted = true;
      } else {
        Alert.alert('Warning', 'Please allow us to open your Camera Roll, or else you will not be able to choose any image.');
        return;
      }
    } else {
      granted = true;
    }

    if (granted) {
      const result = await ImagePicker.launchImageLibraryAsync({
        quality: 0.72,
      });

      if (!result.cancelled) {
        const manipResult = await ImageManipulator.manipulate(
          result.uri,
          [{ resize: { width: 480 } }],
          { format: 'jpeg' },
        );
        this.setState({ [param]: manipResult.uri });
      }
    } else {
      Alert.alert('Error', 'Please check your app permission and allow KEPP to open your Camera Roll to upload an image.');
    }
  }

  _renderMultipleImages({ item }) {
    return (
      <View style={{ marginHorizontal: 5 }}>
        {item ?
          <TouchableOpacity onPress = {() => this._pickImage(item.state)}>
            <UploadImage>
              {this.state[item.state] ?
                <Image
                  source = {{ uri: this.state[item.state] }}
                  style  = {{ width: 130, height: 130, resizeMode: Image.resizeMode.cover }}
                />
                : <Image
                  source = {{ uri: 'https://via.placeholder.com/80x80' }}
                  style  = {{ alignSelf: 'center', width: 80, height: 80 }}
                />
              }
            </UploadImage>
          </TouchableOpacity>: null
        }
      </View>
    );
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading} />
        <Header type="default" bgColor="#234E7F" navigation={this.props.navigation} leftComponent={null} />
        <Modal
          isVisible={this.state.showModal}
          onBackdropPress={() => this.setState({ showModal: false })}
          backdropOpacity={0.5}
        >
          <View style={{ backgroundColor: '#fff', flexWrap: 'wrap', flexDirection: 'column' }}>
            <ModalOption
              onPress={() => this._takePhoto(this.state.handlingImage)}
              underlayColor="#eee"
            >
              <ModalOptionText>Take a photo</ModalOptionText>
            </ModalOption>
            <ModalOption
              onPress={() => this._chooseImage(this.state.handlingImage)}
              underlayColor="#eee"
            >
              <ModalOptionText>Choose Image from Gallery</ModalOptionText>
            </ModalOption>
          </View>
        </Modal>
        <ContentContainer>
          <CloseButton onPress={() => this.props.navigation.navigate('UploadMethod')} />
          <Swiper
            style           = {styles.wrapper}
            ref             = {(c) => { this._swiper = c; }}
            index           = {0}
            dot             = {<SwiperDot />}
            activeDot       = {<SwiperDotActive />}
            paginationStyle = {{ bottom: 30 }}
            loop            = {false}
          >
            <SectionContainer>
              <Title>Name your item</Title>
              <InputContainer>
                <FormInput
                  value                 = {this.state.productName}
                  placeholder           = "MY LEICA CAMERA"
                  placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                  onChangeText          = {productName => this.setState({ productName })}
                  containerStyle        = {styles.inputContainer}
                  inputStyle            = {styles.input}
                  returnKeyType         = "next"
                  autoCapitalize        = "characters"
                  autoCorrect           = {false}
                  underlineColorAndroid = "#fff"
                  onSubmitEditing       = {() => this._nextSlide()}
                />
                {/* {this.state.titleErr ?
                  <WarningIcon>
                    <Ionicons name="md-information-circle" size={32} color="#F87676" />
                  </WarningIcon> : null} */}
                <FormValidationMessage labelStyle={styles.inputError}>{this.state.titleErr}</FormValidationMessage>
              </InputContainer>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer>

            <SectionContainer>
              <Title>Upload or take a picture{'\n'}of your product</Title>
              <TouchableOpacity onPress = {() => this._pickImage('productImage')}>
                <UploadImage>
                  {this.state.productImage ?
                    <Image source={{ uri: this.state.productImage }} style={{ width: 130, height: 130, resizeMode: Image.resizeMode.cover }} />: 
                    <Icon
                      name  = "box"
                      type  = "feather"
                      size  = {48}
                      color = "#4A90E2"
                    />
                  }
                </UploadImage>
              </TouchableOpacity>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer>

            {/* <SectionContainer>
              <Title>Now take pictures or upload{'\n'}your <Bold>Proof of purchase</Bold></Title>
              <TouchableOpacity onPress = {() => this._pickImage('receiptImage')}>
                <UploadImage>
                  {this.state.receiptImage ?
                    <Image source={{ uri: this.state.receiptImage }} style={{ width: 130, height: 130, resizeMode: Image.resizeMode.cover }} />:
                    <Image
                      source = {require('./../../../assets/icons/default/blue/Receipt.png')}
                      style  = {{
                        alignSelf: 'center',
                        width: 50,
                        height: 50,
                        resizeMode: 'contain',
                      }}
                    />
                  }
                </UploadImage>
              </TouchableOpacity>
              <View style={{ height: 150 }}>
                <Carousel
                  ref         = {(c) => { this._proofImages = c; }}
                  data        = {this.state.proofEntries}
                  renderItem  = {this._renderMultipleImages.bind(this)}
                  itemWidth   = {140}
                  sliderWidth = {420}
                />
              </View>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer>

            <SectionContainer>
              <Title>Now take pictures or upload{'\n'}your <Bold>Warranty Card</Bold></Title>
              <TouchableOpacity onPress = {() => this._pickImage('warrantyImage')}>
                <UploadImage>
                  {this.state.warrantyImage ?
                    <Image source={{ uri: this.state.warrantyImage }} style={{ width: 130, height: 130, resizeMode: Image.resizeMode.cover }} />:
                    <Image
                      source = {require('./../../../assets/icons/default/blue/Warranty-card.png')}
                      style  = {{ alignSelf: 'center', width: 50, height: 50, resizeMode: 'contain' }}
                    />
                  }
                </UploadImage>
              </TouchableOpacity>
              <View style={{ height: 150 }}>
                <Carousel
                  ref         = {(c) => { this._warrantyImages = c; }}
                  data        = {this.state.warrantyEntries}
                  renderItem  = {this._renderMultipleImages.bind(this)}
                  itemWidth   = {140}
                  sliderWidth = {420}
                />
              </View>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer> */}

            <SectionContainer style={{ marginTop: -30 }}>
              <Title>Choose your item Categories</Title>
              <CategoriesListContainer>
                <ScrollView
                  showsVerticalScrollIndicator
                  style                 = {{ height: 280 }}
                  indicatorStyle        = "white"
                  contentContainerStyle = {{ flexWrap: 'wrap', width: 300, flexDirection: 'row', alignContent: 'center' }}
                >
                  {
                    categories.map((prop, key) => {
                      return (
                        <CategoryCard key={key}>
                          <TouchableOpacity
                            onPress = {(categoryId) => this._onSelectCategory(prop.id)}
                          >
                            <CategoryImage
                              style = {this.state.categoryId == prop.id ? { backgroundColor: '#139AD6' } : null}
                              // onPress = {(categoryId) => this._onSelectCategory(prop.id)}
                            >
                              <Image
                                source = { prop.whiteIcon }
                                style  = {{ width: 40, height: 40, resizeMode: 'contain' }} />
                            </CategoryImage>
                            <CategoryLabel>{prop.label}</CategoryLabel>
                          </TouchableOpacity>
                        </CategoryCard>
                      );
                    })
                  }
                </ScrollView>
              </CategoriesListContainer>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer>

            <SectionContainer>
              <Title>Warranty Duration</Title>
              <WarrantyContainer>
                <View style={{ marginHorizontal: 10 }}>
                  <Text style={{ color: '#fff', fontFamily: 'gotham-book', fontSize: 18, textAlign: 'center', marginBottom: 5 }}>Years</Text>
                  <View style={{ width: 80, position: 'relative' }}>
                    <PickerSelect
                      hideIcon
                      placeholder   = {{}}
                      items         = {years}
                      onValueChange = {(value) => { this.setState({ warrantyYear: value }); }}
                      style         = {{
                        inputIOS: {
                          color            : '#234E7F',
                          fontSize         : 28,
                          fontFamily       : 'gotham-bold',
                          paddingTop       : 13,
                          paddingHorizontal: 10,
                          paddingLeft      : 15,
                          paddingBottom    : 12,
                          backgroundColor  : 'white',
                        },
                        inputAndroid: {
                          color            : '#234E7F',
                          // fontSize         : 28,
                          // fontFamily       : 'gotham-bold',
                          paddingTop       : 13,
                          paddingHorizontal: 10,
                          paddingLeft      : 15,
                          paddingBottom    : 12,
                          backgroundColor  : 'white',
                        },
                      }}
                      value = {this.state.warrantyYear}
                      ref   = {(el) => { this.yearPicker = el; }}
                    />
                    <Ionicons
                      name    = "ios-arrow-down"
                      size    = {24}
                      color   = "#234E7F"
                      style   = {{ position: 'absolute', top: 13, right: 10 }}
                      onPress = {() => {this.yearPicker.togglePicker();}}
                    />
                  </View>
                </View>
                <View style={{ marginHorizontal: 10 }}>
                  <Text style={{ color: '#fff', fontFamily: 'gotham-book', fontSize: 18, textAlign: 'center', marginBottom: 5 }}>Months</Text>
                  <View style={{ width: 80, position: 'relative' }}>
                    <PickerSelect
                      hideIcon
                      placeholder   = {{}}
                      items         = {months}
                      onValueChange = {(value) => { this.setState({ warrantyMonth: value }); }}
                      style         = {{
                        inputIOS: {
                          color            : '#234E7F',
                          fontSize         : 28,
                          fontFamily       : 'gotham-bold',
                          paddingTop       : 13,
                          paddingHorizontal: 10,
                          paddingLeft      : 15,
                          paddingBottom    : 12,
                          backgroundColor  : 'white',
                        },
                        inputAndroid: {
                          color            : '#234E7F',
                          // fontSize         : 28,
                          // fontFamily       : 'gotham-bold',
                          paddingTop       : 13,
                          paddingHorizontal: 10,
                          paddingLeft      : 15,
                          paddingBottom    : 12,
                          backgroundColor  : 'white',
                        },
                      }}
                      value = {this.state.warrantyMonth}
                      ref   = {(el) => { this.monthPicker = el; }}
                    />
                    <Ionicons
                      name    = "ios-arrow-down"
                      size    = {24}
                      color   = "#234E7F"
                      style   = {{ position: 'absolute', top: 13, right: 10 }}
                      onPress = {() => { this.monthPicker.togglePicker(); }}
                    />
                  </View>
                </View>
                {/* <Text style={{ color: '#fff', fontFamily: 'gotham-book', fontSize: 18, textAlign: 'center' }}> == 0 && this.state.years == 0<{
                  Alert.alert(
                }/Text>
                <Picker
                  selectedValue = {this.state.warrantyMonth}
                  style         = {{ height: 50, width: 100 }}
                  onValueChange = {(value) => this.setState({warrantyMonth: value})}>
                  <Picker.Item label="0" value="0" />
                  <Picker.Item label="1" value="1" />
                  <Picker.Item label="2" value="2" />
                  <Picker.Item label="3" value="3" />
                  <Picker.Item label="4" value="4" />
                  <Picker.Item label="5" value="5" />
                  <Picker.Item label="6" value="6" />
                  <Picker.Item label="7" value="7" />
                  <Picker.Item label="8" value="8" />
                  <Picker.Item label="9" value="9" />
                  <Picker.Item label="10" value="10" />
                  <Picker.Item label="11" value="11" />
                </Picker> */}
                {/* <Slider
                  trackStyle            = {slider.track}
                  thumbStyle            = {slider.thumb}
                  minimumValue          = {0}
                  step                  = {1}
                  maximumValue          = {10}
                  minimumTrackTintColor = 'transparent'
                  value                 = {this.state.duration}
                  onValueChange         = {(duration) => this.setState({duration})} /> */}
              </WarrantyContainer>
              <NextContainer>
                <NextButton nextSlide={() => this._nextSlide()} />
              </NextContainer>
            </SectionContainer>

            <SectionContainer>
              <Title>We need more detail{'\n'}to complete</Title>
              <FormContainer>
                <ScrollView
                  showsVerticalScrollIndicator
                  contentContainerStyle={{ paddingHorizontal: 15 }}
                  indicatorStyle="white"
                >
                  <FormInputContainer>
                    <FormLabel labelStyle={styles.formLabel}>Price (RM)</FormLabel>
                    <FormInput
                      value                 = {this.state.productPrice}
                      placeholder           = "2500"
                      placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                      onChangeText          = {productPrice => this.setState({ productPrice })}
                      containerStyle        = {[styles.inputContainer, {marginLeft: 0, marginRight: 0, borderBottomColor: '#fff'}]}
                      inputStyle            = {[styles.input, { fontFamily: 'gotham-book' }]}
                      keyboardType          = "numeric"
                      returnKeyType         = "next"
                      autoCapitalize        = "none"
                      autoCorrect           = {false}
                      underlineColorAndroid = "#fff"
                      onSubmitEditing       = {() => this.purchaseDatePicker.focus()}
                    />
                    {/* {this.state.productPriceErr ?
                      <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
                    <FormValidationMessage labelStyle={styles.inputError}>{this.state.productPriceErr}</FormValidationMessage> */}
                  </FormInputContainer>

                  <FormInputContainer>
                    <FormLabel labelStyle={styles.formLabel}>Purchase Date</FormLabel>
                    <DatePicker
                      ref            = {(c) => { this.purchaseDatePicker = c; }}
                      style          = {styles.datepicker}
                      date           = {this.state.purchaseDate}
                      mode           = 'date'
                      placeholder    = '2015-12-31'
                      format         = 'YYYY-MM-DD'
                      showIcon       = {false}
                      confirmBtnText = "Confirm"
                      cancelBtnText  = "Cancel"
                      customStyles   = {{
                        dateInput      : styles.dateInput,
                        dateText       : styles.dateText,
                        placeholderText: styles.placeholderText
                      }}
                      onDateChange = {purchaseDate => this.setState({ purchaseDate })}
                    />
                    {/* {this.state.purchaseDateErr ?
                      <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
                    <FormValidationMessage labelStyle={styles.inputError}>{this.state.purchaseDateErr}</FormValidationMessage> */}
                  </FormInputContainer>

                  <FormInputContainer>
                    <FormLabel labelStyle={styles.formLabel}>Shop name and location</FormLabel>
                    <FormInput
                      ref                   = {(c) => { this.locationInput = c; }}
                      value                 = {this.state.shopLocation}
                      placeholder           = "Klang Valley"
                      placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                      onChangeText          = {(shopLocation) => this.setState({shopLocation})}
                      containerStyle        = {[styles.inputContainer, {marginLeft: 0, marginRight: 0, borderBottomColor: '#fff'}]}
                      inputStyle            = {[styles.input, {fontFamily: 'gotham-book'}]}
                      returnKeyType         = "next"
                      autoCapitalize        = "words"
                      autoCorrect           = {false}
                      underlineColorAndroid = "#fff"
                      onSubmitEditing       = {() => this.modelInput.focus()}
                    />
                    {/* {this.state.shopLocationErr ?
                      <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
                    <FormValidationMessage labelStyle={styles.inputError}>{this.state.shopLocationErr}</FormValidationMessage> */}
                  </FormInputContainer>

                  <FormInputContainer>
                    <FormLabel labelStyle={styles.formLabel}>Model name</FormLabel>
                    <FormInput
                      ref                   = {(c) => { this.modelInput = c; }}
                      value                 = {this.state.productModel}
                      placeholder           = "Canon"
                      placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                      onChangeText          = {(productModel) => this.setState({productModel})}
                      containerStyle        = {[styles.inputContainer, {marginLeft: 0, marginRight: 0, borderBottomColor: '#fff'}]}
                      inputStyle            = {[styles.input, {fontFamily: 'gotham-book'}]}
                      returnKeyType         = "next"
                      autoCapitalize        = "words"
                      autoCorrect           = {false}
                      underlineColorAndroid = "#fff"
                      onSubmitEditing       = {() => this.serialNoInput.focus()}
                    />
                    {/* {this.state.productModelErr ?
                      <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
                    <FormValidationMessage labelStyle={styles.inputError}>{this.state.productModelErr}</FormValidationMessage> */}
                  </FormInputContainer>

                  <FormInputContainer>
                    <FormLabel labelStyle={styles.formLabel}>Serial no.</FormLabel>
                    <FormInput
                      ref                   = {(c) => { this.serialNoInput = c; }}
                      value                 = {this.state.seriesNo}
                      placeholder           = "2345432"
                      placeholderTextColor  = "rgba(0, 0, 0, 0.3)"
                      onChangeText          = {(seriesNo) => this.setState({seriesNo})}
                      containerStyle        = {[styles.inputContainer, {marginLeft: 0, marginRight: 0, borderBottomColor: '#fff'}]}
                      inputStyle            = {[styles.input, {fontFamily: 'gotham-book'}]}
                      returnKeyType         = "done"
                      autoCapitalize        = "characters"
                      autoCorrect           = {false}
                      underlineColorAndroid = "#fff"
                    />
                    {/* {this.state.seriesNoErr ?
                      <WarningIcon><Ionicons name="md-information-circle" size={32} color="#F87676" /></WarningIcon> : null}
                    <FormValidationMessage labelStyle={styles.inputError}>{this.state.seriesNoErr}</FormValidationMessage> */}
                  </FormInputContainer>

                  <Button
                    onPress   = {() => this._submit()}
                    rightIcon = {{
                      name : 'arrow-right',
                      type : 'feather',
                      size : 20,
                      style: styles.buttonIcon,
                    }}
                    buttonStyle = {[{ backgroundColor: 'transparent', width: '100%' , marginBottom: 35 }]}
                    textStyle   = {styles.buttonText}
                    title       = "SUBMIT"
                    color       = "#fff"
                  />
                </ScrollView>
              </FormContainer>

              {/* <NextContainer>
                
              </NextContainer> */}
            </SectionContainer>
          </Swiper>

        </ContentContainer>
      </Container>
    );
  }
}

export default Screen;