import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, AsyncStorage, Text, StyleSheet, View, Image } from 'react-native';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { Button, Icon } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import Header from 'components/common/CustomHeader';
import CloseButton from 'components/common/CloseButton';
import Loader from 'components/common/Loader';

const Container = styled.View`
  flex           : 1;
  backgroundColor: #234E7F;
`;

const ContentContainer = styled.View`
  flex          : 1;
  alignItems    : center;
  justifyContent: center;
`;

const WelcomeTitle = styled.Text`
  color     : #fff;
  fontFamily: 'gotham-book';
  fontSize  : 20;
  marginTop : -70;
  textAlign : center;
`;

const Subtitle = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-book';
  fontSize    : 14;
  textAlign   : center;
  marginBottom: 50;
`;

const ChooseText = styled.Text`
  color       : #fff;
  fontFamily  : 'gotham-bold';
  fontSize    : 14;
  textAlign   : center;
  marginBottom: 50;
`;

const MethodsContainer = styled.View`
  flexDirection: row;
`;

const MethodContainer = styled.View`
  alignItems      : center;
  width           : 80;
  marginHorizontal: 10;
`;

const MethodTitle = styled.Text`
  color     : #fff;
  fontFamily: 'gotham-bold';
  fontSize  : 12;
  lineHeight: 13;
  textAlign : center;
`;


const styles = StyleSheet.create({
})

cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

cacheFonts = (fonts) => {
  return fonts.map(font => Font.loadAsync(font));
}

class HomeScreen extends Component {
  state = {
    isReady: true,
  }

  async _loadAssetsAsync() {
    // const imageAssets = cacheImages([
    //   require('./assets/images/circle.jpg'),
    // ]);

    // const fontAssets = cacheFonts([FontAwesome.font]);

    // await Promise.all([...fontAssets]);
    // await Promise.all([...imageAssets, ...fontAssets]);
  }

  constructor(props){
    super(props);
    this.state = {
      loading: false,
      name: null,
    }
  }

  componentWillMount = async () => {
    this.setState({ loading: true });
    try {
      const name = await AsyncStorage.getItem('Kepp.name');
      if (name){
        this.setState({ loading: false });
        this.setState({name})
      } else {
        const token = await AsyncStorage.getItem('Kepp.jwtToken');
        if (token) {
          fetch('https://www.kepp.link/secure/userdetail', {
            method : 'GET',
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
            .then(response => response.json())
            .then(async (responseJson) => {
              await AsyncStorage.setItem('Kepp.name', responseJson.firstName);
              this.setState({
                loading: false,
                name: responseJson.firstName
              });
            })
            .catch(async (err) => {
              await AsyncStorage.removeItem('Kepp.jwtToken');
              this.setState({ loading: false });
              this.props.navigation.navigate('Login');
            });
        } else {
          this.setState({ loading: false });
          this.props.navigation.navigate('Login');
        }
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err));
    }
  };

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <Container>
        <Loader loading={this.state.loading}/>
        <Header type="default" bgColor="#234E7F" navigation={this.props.navigation} leftComponent={null} />
        <ContentContainer>
          <CloseButton onPress = {() => this.props.navigation.navigate("Home")} />
          {/* <View style={{ position: 'absolute', top: 0, right: 5 }}>
            <TouchableOpacity
              onPress = {() => this.props.navigation.navigate("Home")}
            >
              <View style={{ marginLeft: 10, padding: 15 }}>
                <Image
                  source = {require('./../../../assets/icons/default/white/Close.png')}
                  style  = {{ width: 20, height: 20, resizeMode: 'contain', }}
                />
              </View>
            </TouchableOpacity>
          </View> */}
          <WelcomeTitle>Hello{this.state.name ? ` ${this.state.name}` : null},</WelcomeTitle>
          <Subtitle>Let's start organise your warranty</Subtitle>
          <ChooseText>Choose any upload method</ChooseText>
          <MethodsContainer>
            <MethodContainer>
              <TouchableOpacity onPress = {() => this.props.navigation.navigate("QRCode")}>
                <Image 
                  source = {require('./../../../assets/icons/default/white/QR-CODE.png')}
                  style  = {{alignSelf: 'center', marginBottom: 10, width: 50, height: 50}}
                />
                <MethodTitle>QR{"\n"}CODE</MethodTitle>
              </TouchableOpacity>
            </MethodContainer>
            <MethodContainer>
              <TouchableOpacity onPress = {() => this.props.navigation.navigate("ScanReceipt")}>
                <Image 
                  source = {require('./../../../assets/icons/default/white/Receipt.png')}
                  style  = {{alignSelf: 'center', marginBottom: 10, width: 50, height: 50}}
                />
                <MethodTitle>SCAN{"\n"}RECEIPT</MethodTitle>
              </TouchableOpacity>
            </MethodContainer>
            <MethodContainer>
              <TouchableOpacity onPress = {() => this.props.navigation.navigate("ManualUpload")}>
                <Image 
                  source = {require('./../../../assets/icons/default/white/Manual-up.png')}
                  style  = {{alignSelf: 'center', marginBottom: 10, width: 50, height: 50}}
                />
                <MethodTitle>MANUAL{"\n"}UPLOAD</MethodTitle>
              </TouchableOpacity>
            </MethodContainer>
          </MethodsContainer>
        </ContentContainer>
      </Container>
    );
  }
}

export default HomeScreen;