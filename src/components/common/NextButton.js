import React, { Component } from 'react';
import { Platform } from 'react-native';
import { Button } from 'react-native-elements';

class NextButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Button
        onPress   = {this.props.nextSlide}
        rightIcon = {{
          name : 'arrow-right',
          type : 'feather',
          size : 20,
          style: {
            ...Platform.select({
              ios: {
                marginLeft: 5,
                marginTop : -2,
              },
              android: {
                marginLeft: 5,
                marginTop : 0,
              },
            }),
          },
        }}
        buttonStyle = {[{ backgroundColor: 'transparent', width: '100%' }]}
        title       = "NEXT"
        color       = "#fff"
      />
    );
  }
}

export default NextButton;
