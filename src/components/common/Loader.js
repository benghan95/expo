import React, { Component } from 'react';
import { StyleSheet, View, Modal, Dimensions, ActivityIndicator, Image } from "react-native";

class Loader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Modal
        transparent
        animationType  = "none"
        visible        = {this.props.loading}
        onRequestClose = {() => {}}
      >
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <Image
              source     = {require('./../../../assets/images/logo-animation.gif')}
              style      = {{ width: 80, height: 88, resizeMode: 'contain' }} />
          </View>
        </View>
      </Modal>
    );
  }
}

export default Loader;

const styles = StyleSheet.create({
  modalBackground: {
    flex           : 1,
    alignItems     : 'center',
    flexDirection  : 'column',
    justifyContent : 'space-around',
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height         : 130,
    width          : 130,
    borderRadius   : 130,
    display        : 'flex',
    alignItems     : 'center',
    justifyContent : 'space-around'
  }
});
