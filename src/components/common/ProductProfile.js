import React, { Component } from 'react';
import { Alert, Platform, TouchableOpacity, Text, StyleSheet, View, Image, ScrollView, Dimensions, Animated } from 'react-native';
import Expo, { Font, AppLoading, Asset } from 'expo';
import { Button, Icon } from 'react-native-elements';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import moment from 'moment';

const { width, height } = Dimensions.get('window');

const DisplayContainer = styled.View`
  backgroundColor: ${Colors.Default.Cyan};
`;

const ContentContainer = styled.View`
  paddingHorizontal: 30;
  paddingTop       : 10;
  paddingBottom    : 30;
`;

const ContentLabel = styled.Text`
  color     : #fff;
  fontFamily: 'gotham-bold';
  fontSize  : 14;
  lineHeight: 18;
  opacity   : 0.5;
`;

const ContentValue = styled.Text`
  color     : #fff;
  fontFamily: 'gotham-bold';
  fontSize  : 16;
  lineHeight: 18;
`;

const ProductTitleContainer = styled.View`
  flexDirection: row;
  alignItems   : center;
  marginTop    : 5;
`;

const CategoryImage = styled.Image`
  width     : 30;
  height    : 30;
  resizeMode: contain;
`;

const ProductTitle = styled.Text`
  color     : #fff;
  fontFamily: 'gotham-medium';
  fontSize  : 20;
  lineHeight: 20;
  marginLeft: 10;
  marginTop : 3;
`;

const Line = styled.View`
  borderTopWidth: 1;
  borderTopColor: rgba(255, 255, 255, 0.5);
  marginVertical: 30;
`;

const DescContainer = styled.View`
  flexDirection : row;
  justifyContent: space-between;
`;

const DetailsContainer = styled.View`
`;

const PurchaseDateContainer = styled.View`
  marginBottom: 25;
`;

const WarrantyContainer = styled.View`
`;

const ProductImage = styled.View`
  alignItems     : center;
  overflow       : hidden;
  justifyContent : center;
  backgroundColor: #D8D8D8;
  borderRadius   : 110;
  height         : 110;
  width          : 110;
`;

const ActionsContainer = styled.View`
  backgroundColor: ${Colors.Default.Blue};
  flexDirection  : row;
  justifyContent : space-around;
`;

const ActionText = styled.Text`
  color            : #fff;
  fontSize         : 14;
  fontFamily       : 'gotham-bold';
  lineHeight       : 18;
  paddingHorizontal: 20;
  paddingVertical  : 20;
`;

// const styles = StyleSheet.create({
// })

// cacheImages = (images) => {
//   return images.map(image => {
//     if (typeof image === 'string') {
//       return Image.prefetch(image);
//     } else {
//       return Asset.fromModule(image).downloadAsync();
//     }
//   });
// }

// cacheFonts = (fonts) => {
//   return fonts.map(font => Font.loadAsync(font));
// }

class ProductProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
    };
  }

  componentDidMount() {
  }

  render() {
    // if (!this.state.isReady) {
    //   return (
    //     <AppLoading
    //       startAsync = {this._loadAssetsAsync}
    //       onFinish   = {() => this.setState({ isReady: true })}
    //       onError    = {console.warn}
    //     />
    //   );
    // }

    return (
      <DisplayContainer>
        <ContentContainer>
          <ContentLabel>{this.props.data.category ? this.props.data.category.label.toUpperCase() : '-'}</ContentLabel>
          <ProductTitleContainer>
            {this.props.data.category ?
              <CategoryImage
                source = {this.props.data.category.whiteIcon}
              /> : null
            }
            <ProductTitle>{this.props.data.title ? this.props.data.title.toUpperCase() : '-'}</ProductTitle>
          </ProductTitleContainer>
          <Line />
          <DescContainer>
            <DetailsContainer>
              <PurchaseDateContainer>
                <ContentLabel>Date of Purchase</ContentLabel>
                <ContentValue>{this.props.data.purchaseDate ? moment(this.props.data.purchaseDate).format('DD . MM . YYYY') : '-'}</ContentValue>
              </PurchaseDateContainer>
              <WarrantyContainer>
                <ContentLabel>Warranty Period</ContentLabel>
                <ContentValue>{this.props.data.warrantyPeriod ? `Ending ${moment(this.props.data.warrantyPeriod).format('DD/MM/YYYY')}` : '-'}</ContentValue>
              </WarrantyContainer>
            </DetailsContainer>
            <ProductImage>
              {this.props.data.productImage ?
                <Image source={{ uri: this.props.data.productImage }} style={{ width: 110, height: 110, resizeMode: 'cover' }} /> :
                <Image
                  source = {{ uri: 'https://via.placeholder.com/110x110'}}
                  style  = {{
                    alignSelf: 'center', width: 110, height: 110, resizeMode: 'cover',
                  }}
                />
              }
            </ProductImage>
          </DescContainer>
        </ContentContainer>
        <ActionsContainer>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { tab: 'detail', data: this.props.data })}>
            <ActionText>DETAIL</ActionText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { tab: 'sell', data: this.props.data })}>
            <ActionText>SELL</ActionText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { tab: 'protection', data: this.props.data })}>
            <ActionText>PROTECTION</ActionText>
          </TouchableOpacity>
        </ActionsContainer>
      </DisplayContainer>
    );
  }
}

export default ProductProfile;