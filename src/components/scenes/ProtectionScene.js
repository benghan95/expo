import React, { Component } from 'react';
import { Platform, TouchableOpacity, TouchableHighlight, Text } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Font, AppLoading, Asset } from 'expo';
import { View, StyleSheet, Image } from 'react-native';
import { FontAwesome, Feather, Ionicons } from '@expo/vector-icons';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import * as Progress from 'react-native-progress';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import moment from 'moment';

const Container = styled.View`
  flex             : 1;
  backgroundColor  : #fff;
  paddingTop       : 50;
  paddingBottom    : 15;
  paddingHorizontal: 20;
`;

const ProtectionContainer = styled.View`
  justifyContent: center;
  alignItems    : center;
  marginBottom  : 30;
`;

const ProgressBorder = styled.View`
  height         : 215;
  width          : 215;
  borderStyle    : dotted;
  borderRadius   : 150;
  borderWidth    : 4;
  borderColor    : #E0E2E1;
  backgroundColor: #fff;
  position       : absolute;
`;

const ExpiryContainer = styled.View`
  position: absolute;
`;

const ComingSoonTitle = styled.Text`
  fontFamily: 'gotham-bold';
  fontSize  : 20;
  color     : #333;
  marginBottom: 20;
  textAlign   : center;
`;

const ComingSoonDescription = styled.Text`
  fontFamily  : 'gotham-book';
  fontSize    : 14;
  color       : #3E3E3E;
  textAlign   : center;
  marginBottom: 40;
`;

const ComingSoonSubtitle = styled.Text`
  fontFamily  : 'gotham-bold';
  fontSize    : 16;
  lineHeight: 18;
  color       : #555;
  textAlign   : center;
  marginBottom: 40;
`;


const Label = styled.Text`
  fontFamily: 'gotham-book';
  fontSize  : 14;
  color     : #0D0D0D;
`;

const Value = styled.Text`
  fontFamily: 'gotham-medium';
  fontSize  : 20;
  color     : #605959;
`;

const Description = styled.Text`
  fontFamily  : 'gotham-book';
  fontSize    : 14;
  color       : #3E3E3E;
  textAlign   : center;
  marginBottom: 40;
`;

const ActionsContainer = styled.View`
  flexDirection: row;
  alignItems   : center;
`;

const ActionContainer = styled.View`
  backgroundColor: ${Colors.Default.Cyan}
  borderWidth    : 1;
  borderColor    : #fff;
  width          : 50%;
`;

const ActionUpperContainer = styled.View`
  paddingVertical  : 20;
  paddingHorizontal: 10;
`;

const ActionTitle = styled.Text`
  fontFamily: 'gotham-bold';
  fontSize  : 12;
  color     : #fff;
  textAlign : center;
`;

const TitleContainer = styled.View`
  justifyContent: center;
  height        : 150;
`;

const ActionValue = styled.Text`
  fontFamily: 'gotham-bold';
  fontSize  : 60;
  color     : #fff;
  textAlign : center;
`;

const ActionBigTitle = styled.Text`
  fontFamily: 'gotham-bold';
  fontSize  : 20;
  color     : #fff;
  textAlign : center;
`;

const ActionDesc = styled.Text`
  fontFamily: 'gotham-book';
  fontSize  : 12;
  color     : #fff;
  textAlign : center;
`;

const ActionButton = styled.TouchableHighlight`
  backgroundColor  : ${Colors.Default.DarkCyan}
  paddingHorizontal: 15;
  paddingVertical  : 15;
  alignItems       : center;
  justifyContent   : center;
`;

const ButtonText = styled.Text`
  fontFamily: 'gotham-bold';
  fontSize  : 16;
  color     : #fff;
  textAlign : center;
`;


const styles = StyleSheet.create({
  progressBar: {
    transform: [{scaleX: -1}],
  },
});

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      protectionDays: 0,
      warrantyDays: 0,
    };
  }

  componentDidMount = () => {
    if (this.props.entry) {
      const { entry } = this.props;
      // const purchaseDate = moment().subtract(200, 'days');
      const purchaseDate = moment(entry.purchaseDate);
      let warrantyUsed = 0;
      if (entry.warrantyDuration) {
        const { warrantyDuration } = entry;
        purchaseDate.add(warrantyDuration, 'days');
        warrantyUsed = purchaseDate.diff(moment(), 'days');
        let warrantyDays = Math.round(((warrantyDuration - warrantyUsed) / warrantyDuration) * 100);
        if (warrantyDays > 100) {
          warrantyDays = 100;
        }
        this.setState({ warrantyDays });
      }
      if (entry.protectionDuration) {
        const { protectionDuration } = entry;
        purchaseDate.add(protectionDuration, 'days');
        const protectionUsed = purchaseDate.diff(moment(), 'days');
        let protectionDays = Math.round(((protectionUsed - warrantyUsed) / protectionDuration) * 100)
        if (protectionDays > 100) {
          protectionDays = 100;
        } else if (protectionDays < 0) {
          protectionDays = 0;
        }
        this.setState({ protectionDays });
      }
    }
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.entry) {
      const { entry } = nextProps;
      // const purchaseDate = moment().subtract(200, 'days');
      const purchaseDate = moment(entry.purchaseDate);
      let warrantyUsed = 0;
      if (entry.warrantyDuration) {
        const { warrantyDuration } = entry;
        purchaseDate.add(warrantyDuration, 'days');
        warrantyUsed = purchaseDate.diff(moment(), 'days');
        let warrantyDays = Math.round(((warrantyDuration - warrantyUsed) / warrantyDuration) * 100);
        if (warrantyDays > 100) {
          warrantyDays = 100;
        }
        this.setState({ warrantyDays });
      }
      if (entry.protectionDuration) {
        const { protectionDuration } = entry;
        purchaseDate.add(protectionDuration, 'days');
        const protectionUsed = purchaseDate.diff(moment(), 'days');
        let protectionDays = Math.round(((protectionUsed - warrantyUsed) / protectionDuration) * 100)
        if (protectionDays > 100) {
          protectionDays = 100;
        } else if (protectionDays < 0) {
          protectionDays = 0;
        }
        this.setState({ protectionDays });
      }
    }
  };

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync = {this._loadAssetsAsync}
          onFinish   = {() => this.setState({ isReady: true })}
          onError    = {console.warn}
        />
      );
    }

    if (this.props.isAdmin) {
      return (
        <Container>
          <ProtectionContainer>
            <View style={styles.progressBar}>
              <ProgressBorder />
              <AnimatedCircularProgress
                rotation        = {0}
                arcSweepAngle   = {360}
                size            = {215}
                style           = {{backgroundColor: 'transparent'}}
                width           = {4}
                fill            = {this.state.protectionDays}
                linecap         = "round"
                tintColor       = {Colors.Default.Green}
                backgroundColor = '#E0E2E1' />
            </View>
            <View style={[styles.progressBar, {position: 'absolute', justifyContent: 'center', alignItems: 'center'}]}>
              <ProgressBorder style={{height: 200, width: 200,}} />
              <AnimatedCircularProgress
                rotation        = {0}
                arcSweepAngle   = {360}
                size            = {200}
                style           = {{backgroundColor: 'transparent'}}
                width           = {4}
                fill            = {this.state.warrantyDays}
                linecap         = "round"
                tintColor       = {Colors.Default.Cyan}
                backgroundColor = '#E0E2E1' />
            </View>
            <ExpiryContainer>
              <Label>Warranty Expire</Label>
              <Value>{ this.props.data ? moment(this.props.data.warrantyPeriod).format('DD/MM/YYYY') : '-'}</Value>
            </ExpiryContainer>
          </ProtectionContainer>
          <Description>Extend your Warranty with us{'\n'}SECURE, FAST and HASSLE FREE</Description>

          <ActionsContainer>
            <ActionContainer>
              <ActionUpperContainer>
                <ActionTitle>WARRANTY</ActionTitle>
                <TitleContainer>
                  <ActionBigTitle>STANDARD{'\n'}WARRANTY</ActionBigTitle>
                </TitleContainer>
                <ActionDesc>Extend your warranty{'\n'}life securely with our{'\n'}insurance company</ActionDesc>
              </ActionUpperContainer>
              <ActionButton>
                <ButtonText>EXTEND{'\n'}WARRANTY</ButtonText>
              </ActionButton>
            </ActionContainer>
            <ActionContainer style={{backgroundColor: Colors.Default.Green}}>
              <ActionUpperContainer>
                <ActionTitle>EXTRA PROTECTION</ActionTitle>
                <TitleContainer>
                  <ActionValue>0</ActionValue>
                  <ActionBigTitle>PROTECTION{'\n'}PLAN</ActionBigTitle>
                </TitleContainer>
                <ActionDesc>You dont have Extra{'\n'}Protection plan for {'\n'}
                <Text style={{ fontFamily: 'gotham-medium' }}>MY LEICA CAMERA</Text></ActionDesc>
              </ActionUpperContainer>
              <ActionButton style={{backgroundColor: Colors.Default.DarkGreen}}>
                <ButtonText>GET EXTRA{'\n'}PROTECTION</ButtonText>
              </ActionButton>
            </ActionContainer>
          </ActionsContainer>
        </Container>
      );
    }

    return (
      <Container>
        <ComingSoonTitle>COMING SOON</ComingSoonTitle>
        <ComingSoonDescription>Extend your product warranty or{'\n'}get extra protection with KEPP{'\n'}SECURE, FAST AND HASSLE FREE</ComingSoonDescription>
      </Container>
    );
  }
}

export default Scene;