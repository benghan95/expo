import React, { Component } from 'react';
import { Platform, TouchableOpacity, TouchableHighlight, Text, TextInput, Alert, AsyncStorage } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Font, AppLoading, Asset } from 'expo';
import { View, StyleSheet, Image } from 'react-native';
import { FontAwesome, Feather, Ionicons } from '@expo/vector-icons';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import CloseButton from 'components/common/CloseButton';
import Loader from 'components/common/Loader';
import * as Progress from 'react-native-progress';
import ImageView from 'react-native-image-view';
import PickerSelect from 'react-native-picker-select';
import moment from 'moment';

const Container = styled.View`
  flex             : 1;
  backgroundColor  : #fff;
  paddingTop       : 50;
  paddingBottom    : 15;
  paddingHorizontal: 20;
  position         : relative;
`;

const WarrantyContainer = styled.View`
  alignItems  : center;
  marginBottom: 15;
`;

const WarrantyTitle = styled.Text`
  color       : #0D0D0D;
  fontFamily  : 'gotham-book';
  fontSize    : 14;
  opacity     : 0.5;
  marginBottom: 10;
`;

const WarrantyDaysLeft = styled.Text`
  color     : #498CDA;
  fontFamily: 'gotham-book';
  fontSize  : 48;
  marginBottom: 5;
`;

const WarrantySubtitle = styled.Text`
  color     : #0D0D0D;
  fontFamily: 'gotham-book';
  fontSize  : 14;
  marginTop : -15;
  opacity   : 0.5;
`;

const ProgressContainer = styled.View`
  paddingHorizontal: 5;
`;

const DurationContainer = styled.View`
  justifyContent: space-between;
  flexDirection : row;
  marginTop     : 10;
`;

const DateContainer = styled.View`
`;

const YearText = styled.Text`
  color     : #797979;
  fontFamily: 'gotham-book';
  fontSize  : 24;
`;

const DateText = styled.Text`
  color     : #797979;
  fontFamily: 'gotham-book';
  fontSize  : 12;
  marginTop : -5;
`;

const DetailsContainer = styled.View`
  paddingTop: 30;
`;

const ItemContainer = styled.View`
  borderBottomWidth: 1;
  borderBottomColor: #D6D6D6;
  paddingVertical  : 30;
`;

const DataContainer = styled.View`
  justifyContent: center;
`;

const DataLabel = styled.Text`
  color       : #4D4D4F;
  fontFamily  : 'gotham-book';
  fontSize    : 15;
  marginBottom: 5;
  opacity     : 0.5;
  textAlign   : center;
`;

const DataValue = styled.Text`
  color     : #4D4D4F;
  fontFamily: 'gotham-book';
  fontSize  : 20;
  textAlign : center;
`;

const RMText = styled.Text`
  fontSize: 14;
`;

const ActionsContainer = styled.View`
  marginTop   : 25;
  marginBottom: 30;
`;

const ActionButton = styled.TouchableHighlight`
  backgroundColor  : #139AD6;
  borderWidth      : 1;
  borderColor      : #fff;
  paddingHorizontal: 20;
  ${'' /* paddingVertical  : 45; */}
  height: 120;
  justifyContent: center;
  alignItems       : center;
`;

const ActionText = styled.Text`
  color     : #fff;
  fontFamily: 'gotham-bold';
  fontSize  : 16;
  lineHeight: 17;
  textAlign : center;
`;

const SmallNote = styled.Text`
  color     : #ccc;
  fontFamily: 'gotham-bold';
  fontSize  : 13;
  lineHeight: 17;
  textAlign : center;
`;

const ClaimHeader = styled.View`
  ${'' /* alignItems: center; */}
  position: relative;
`;

const ClaimTitle = styled.Text`
  color     : #333;
  fontFamily: 'gotham-bold';
  fontSize  : 24;
  lineHeight: 28;
  textAlign : center;
  marginBottom: 50;
`;

const ClaimFormContainer = styled.View`
  paddingBottom: 150;
`;

const ClaimLabel = styled.Text`
  color       : #333;
  fontFamily  : 'gotham-book';
  fontSize    : 16;
  textAlign   : center;
  marginBottom: 10;
`;

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: Colors.Default.Cyan,
    paddingTop     : 20,
    paddingBottom  : 20,
  },
  buttonText: {
    color     : '#ffffff',
    fontSize  : 18,
    textAlign : 'center',
    fontFamily: 'gotham-bold',
  },
  button: {
    paddingTop   : 20,
    paddingBottom: 18,
    marginBottom : 8,
  },
});

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady           : true,
      loading           : false,
      isImageViewVisible: false,
      receiptImage      : null,
      productData       : null,
      toClaim           : false,
      failureType       : '',
      failureDesc       : '',
      failureDescOnFocus: false,
      receiptImages     : [],
    };
  }

  componentDidMount() {
    if (this.props.entry) {
      if (this.props.entry.receiptImage) {
        this.setState({
          receiptImages: [{
            source: { uri: `https://www.kepp.link/unsecure/getimage/${this.props.entry.receiptImage}` },
          }],
        });
      }
    }

    if (this.props.data) {
      this.setState({
        productData: this.props.data,
      });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.entry) {
      if (nextProps.entry.receiptImage) {
        this.setState({
          receiptImages: [{
            source: { uri: `https://www.kepp.link/unsecure/getimage/${nextProps.entry.receiptImage}` },
          }],
        });
      }
    }

    if (nextProps.data) {
      this.setState({
        productData: nextProps.data,
      });
    }
  };

  _claimWarranty = async (entry) => {
    this.setState({ loading: true });

    if (!this.state.failureType) {
      Alert.alert('Please enter type of your product failure.');
      this.setState({ loading: false });
      return false;
    }

    if (!this.state.failureDesc) {
      Alert.alert('Please enter a short description / some symptoms on your product failure.');
      this.setState({ loading: false });
      return false;
    }

    const token = await AsyncStorage.getItem('Kepp.jwtToken');
    const data = {
      itemId     : entry.itemId,
      failureType: this.state.failureType,
      failureDesc: this.state.failureDesc,
    };

    fetch('https://www.kepp.link/secure/claim', {
      method : 'POST',
      headers: {
        Authorization : `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({ loading: false });
        if (responseJson.statusResp) {
          const responseCode = parseInt(responseJson.statusResp.responseCode, 10);
          if (responseCode >= 100000 && responseCode < 200000) {
            Alert.alert('Claim request has been submitted, please check your email inbox on the status.', '', [
              {
                text: 'OK',
                onPress: () => {
                  this.setState({
                    toClaim: false,
                    failureType: null,
                    failureDesc: null,
                  });
                },
              },
            ]);
          } else {
            Alert.alert(responseJson.statusResp.responseDesc);
          }
        }
      })
      .catch((err) => {
        this.setState({ loading: false });
        Alert.alert(JSON.stringify(err));
      });
  }

  _deleteItem = (entry) => {
    Alert.alert(
      'Delete Item',
      'Are you sure you want to delete this item?\n(Once deleted it will not be able to revert back)',
      [
        {
          text: 'Confirm Delete',
          onPress: async () => {
            this.setState({ loading: true });
            const token = await AsyncStorage.getItem('Kepp.jwtToken');
            fetch(`https://www.kepp.link/secure/entries/${entry.itemId}`, {
              method : 'DELETE',
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
              .then(response => response.json())
              .then((responseJson) => {
                this.setState({ loading: false });
                const responseCode = parseInt(responseJson.responseCode, 10);
                if (responseCode >= 100000 && responseCode < 200000) {
                  Alert.alert('Item has been deleted.', null, [
                    {
                      text: 'OK',
                      onPress: () => {
                        this.props.navigation.navigate('Home');
                      }
                    }
                  ]);
                } else {
                  Alert.alert('Error', responseJson.responseDesc);
                }
              })
              .catch((err) => {
                this.setState({ loading: false });
                Alert.alert(JSON.stringify(err));
              });
          },
        },
        {
          text: 'Cancel',
        },
      ],
    );
  }

  render() {
    if (this.state.toClaim) {
      return (
        <Container>
          <Loader loading={this.state.loading} />
          <ClaimHeader>
            <Icon
              name           = 'arrow-left'
              type           = 'feather'
              size           = {32}
              color          = '#333'
              containerStyle = {{ position: 'absolute', zIndex: 5, left: -10, top: -15 }}
              iconStyle      = {{ padding: 10 }}
              onPress        = {() => this.setState({ toClaim: false })}
            />
            <ClaimTitle>CLAIM WARRANTY</ClaimTitle>
          </ClaimHeader>
          <ClaimFormContainer>
            <View>
              <ClaimLabel>Failure Type</ClaimLabel>
              <TextInput
                ref           = {(c) => { this.failureTypeInput = c; }}
                placeholder   = "Parts not functioning"
                onChangeText  = {failureType => this.setState({ failureType })}
                returnKeyType = "next"
                style         = {{
                  fontSize: 16,
                  fontFamily: 'gotham-book',
                  borderWidth: 1,
                  borderColor: '#999',
                  paddingHorizontal: 10,
                  paddingTop: 10,
                  paddingBottom: 10,
                  marginBottom: 30,
                  marginHorizontal: 15,
                  shadowColor  : '#000000',
                  shadowOpacity: 0.1,
                  shadowOffset : {
                    width : 0,
                    height: 1,
                  },
                  shadowRadius: 2,
                  elevation   : 1,
                  height: 40,
                }}
                onSubmitEditing = {() => this.failureDescInput.focus()}
              />
            </View>
            <View>
              <ClaimLabel>Description</ClaimLabel>
              <TextInput
                multiline
                ref           = {(c) => { this.failureDescInput = c; }}
                placeholder   = "Tell us more..."
                onChangeText  = {failureDesc => this.setState({ failureDesc })}
                style         = {{
                  fontSize: 16,
                  fontFamily: 'gotham-book',
                  borderWidth: 1,
                  borderColor: '#999',
                  paddingHorizontal: 15,
                  paddingTop: 15,
                  paddingBottom: 15,
                  marginBottom: 30,
                  marginHorizontal: 15,
                  shadowColor  : '#000000',
                  shadowOpacity: 0.3,
                  shadowOffset : {
                    width : 0,
                    height: 1,
                  },
                  shadowRadius: 2,
                  elevation   : 1,
                  height: 200,
                }}
                onFocus={() => this.setState({ failureDescOnFocus: true })}
              />
            </View>
            <Button
              onPress        = {() => this._claimWarranty(this.props.entry)}
              buttonStyle    = {styles.buttonContainer}
              textStyle      = {styles.buttonText}
              title          = "SUBMIT CLAIM"
              backgroudColor = {Colors.Default.Cyan}
            />
          </ClaimFormContainer>
        </Container>
      );
    }

    return (
      <Container>
        <Loader loading={this.state.loading} />
        <WarrantyContainer>
          <WarrantyTitle>Warranty Period</WarrantyTitle>
          <WarrantyDaysLeft>{this.state.productData ? this.state.productData.daysLeft : null}</WarrantyDaysLeft>
          <WarrantySubtitle>days left</WarrantySubtitle>
        </WarrantyContainer>
        <ProgressContainer>
          <Progress.Bar
            color         = "#047CED"
            progress      = {this.state.productData ? (this.state.productData.percentage / 100) : 0}
            height        = {4}
            width         = {null}
            borderRadius  = {0}
            unfilledColor = "#D8D8D8"
            borderWidth   = {0}
          />
          <DurationContainer>
            <DateContainer>
              <YearText>{ this.props.entry ? moment(this.props.entry.purchaseDate).format('YYYY') : '-'}</YearText>
              <DateText>{ this.props.entry ? moment(this.props.entry.purchaseDate).format('DD MMMM') : '-'}</DateText>
            </DateContainer>
            <DateContainer style={{alignItems: 'flex-end'}}>
              <YearText>{ this.state.productData ? (this.state.productData.warrantyPeriod ? moment(this.state.productData.warrantyPeriod).format('YYYY') : '-') : '-'}</YearText>
              <DateText>{ this.state.productData ? (this.state.productData.warrantyPeriod ? moment(this.state.productData.warrantyPeriod).format('DD MMMM') : '-') : '-'}</DateText>
            </DateContainer>
          </DurationContainer>
        </ProgressContainer>
        <DetailsContainer>
          <ItemContainer style={{ flexDirection: 'row' }}>
            <DataContainer style={{ width: '50%', borderRightWidth: 1, borderRightColor: '#D6D6D6' }}>
              <DataLabel style={{ fontSize: 13, }}>Price</DataLabel>
              <DataValue>
                { this.props.entry ? <RMText>RM</RMText> : null }
                <Text style={{ fontSize: 30 }}>{ this.props.entry ? this.props.entry.purchasePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : '-'}</Text>
              </DataValue>
            </DataContainer>
            <DataContainer style={{ width: '50%' }}>
              <DataLabel style={{ fontSize: 13, }}>Current Value</DataLabel>
              <DataValue>
                {/* <RMText>RM</RMText> */}
                <Text style={{ fontSize: 30, }}>-</Text>
              </DataValue>
            </DataContainer>
          </ItemContainer>
          <ItemContainer>
           <DataLabel>Date of Purchase</DataLabel>
            <DataValue>{ this.state.productData ? moment(this.state.productData.warrantyPeriod).format('DD-MM-YYYY') : '-'}</DataValue>
          </ItemContainer>
          <ItemContainer>
           <DataLabel>Model</DataLabel>
            <DataValue>{ this.props.entry ? this.props.entry.model : '-'}</DataValue>
          </ItemContainer>
          <ItemContainer>
           <DataLabel>Series no.</DataLabel>
            <DataValue>{ this.props.entry ? this.props.entry.serialNo : '-'}</DataValue>
          </ItemContainer>
          <ItemContainer style={{ borderBottomWidth: 0 }}>
           <DataLabel>Purchase from</DataLabel>
            <DataValue>{ this.props.entry ? this.props.entry.purchaseLocation : '-'}</DataValue>
          </ItemContainer>
        </DetailsContainer>
        {
          this.state.receiptImages.length > 0 ?
            <Button
              onPress     = {() => this.setState({ isImageViewVisible: true})}
              buttonStyle = {{
                borderWidth      : 1,
                borderColor      : '#979797',
                backgroundColor  : 'transparent',
                paddingTop       : 17,
                paddingVertical  : 15,
                paddingHorizontal: 20,
              }}
              containerViewStyle = {{ marginLeft: 0, marginRight: 0 }}
              textStyle          = {{ fontFamily: 'gotham-medium', fontSize: 14 }}
              title              = "VIEW PROOF OF PURCHASE"
              color              = "#5E5E5E"
            /> : null
        }
        {this.state.receiptImages.length > 0 ?
          <ImageView
            images={this.state.receiptImages}
            imageIndex={0}
            isVisible={this.state.isImageViewVisible}
          /> : null
        }
        {/* <ReceiptContainer>
          <CloseButton onPress={() => this.props.navigation.navigate('UploadMethod')} />
          <Image
            source = {this.state.entry ? { uri: `https://www.kepp.link/unsecure/getimage/${this.state.entry.receiptImage}` } : null}
            style  = {{ backgroundColor: '#ccc', width: '100%', height: '100%', resizeMode: 'contain' }}
          />
        </ReceiptContainer> */}

        <ActionsContainer>
          <View style={{ flexDirection: 'row', }}>
            <ActionButton
              style         = {{ width: '50%' }}
              onPress       = {() => this.setState({ toClaim: true })}
              underlayColor = "#1FA5EE"
            >
              <ActionText>CLAIM{'\n'}WARRANTY</ActionText>
            </ActionButton>
            <ActionButton
              style         = {{ width: '50%', opacity: 0.8, }}
              underlayColor = "#1FA5EE"
            >
              <ActionText>EXTRA{'\n'}PROTECTION{'\n'}<SmallNote>COMING SOON</SmallNote></ActionText>
            </ActionButton>
          </View>
          {/* <View>
            <ActionButton>
              <ActionText>INFOMATION{'\n'}& SUPPORT</ActionText>
            </ActionButton>
          </View> */}
        </ActionsContainer>

        <TouchableOpacity
          style={{ paddingVertical: 15, paddingHorizontal: 25 }}
          onPress={() => this._deleteItem(this.props.entry)}
        >
          <Text style={{ fontFamily: 'gotham-bold', textAlign: 'center', fontSize: 16, color: '#F87878', marginBottom: 30 }}>DELETE ITEM</Text>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default Scene;