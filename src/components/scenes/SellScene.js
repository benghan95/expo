import React, { Component } from 'react';
import { Platform, TouchableOpacity, TouchableHighlight, Text } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Font, AppLoading, Asset } from 'expo';
import { View, StyleSheet, Image } from 'react-native';
import { FontAwesome, Feather, Ionicons } from '@expo/vector-icons';
import { Colors } from 'constants/styles';
import styled from 'styled-components';
import * as Progress from 'react-native-progress';
import Switch from 'react-native-switch-pro';

const Container = styled.View`
  backgroundColor  : #fff;
  paddingTop       : 50;
  paddingBottom    : 15;
  paddingHorizontal: 20;
`;

const ComingSoonTitle = styled.Text`
  fontFamily: 'gotham-bold';
  fontSize  : 20;
  color     : #333;
  marginBottom: 20;
  textAlign   : center;
`;

const ComingSoonDescription = styled.Text`
  fontFamily  : 'gotham-book';
  fontSize    : 14;
  color       : #3E3E3E;
  textAlign   : center;
  marginBottom: 30;
`;

const ComingSoonSubtitle = styled.Text`
  fontFamily  : 'gotham-bold';
  fontSize    : 16;
  lineHeight: 18;
  color       : #555;
  textAlign   : center;
  marginBottom: 40;
`;

const IconContainer = styled.View`
  marginBottom: 30;
  alignItems  : center;
`;

const DescContainer = styled.View`
`;

const Description = styled.Text`
  fontFamily       : 'gotham-book';
  fontSize         : 14;
  color            : #3E3E3E;
  textAlign        : center;
  paddingHorizontal: 20;
`;

const ActionsContainer = styled.View`
  marginTop        : 25;
  marginBottom     : 40;
  paddingHorizontal: 15;
`;

const MarketplaceCard = styled.View`
  flexDirection : row;
  justifyContent: space-between;
  alignItems    : center;
  marginBottom  : 25;
`;

const styles = StyleSheet.create({

});

const options = [
  { label: '', value: '' },
  { label: '', value: '0' },
];

class Scene extends Component {
  state = {
    isReady: true,
  };

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync = {this._loadAssetsAsync}
          onFinish   = {() => this.setState({ isReady: true })}
          onError    = {console.warn}
        />
      );
    }

    if (this.props.isAdmin) {
      return (
        <Container>
          <IconContainer>
            <Image
              source = {require('./../../../assets/images/shopping-cart-moving-symbol.png')}
              style  = {{alignSelf: 'center', width: 70, height: 70, resizeMode: 'contain'}}
            />
          </IconContainer>
          <DescContainer>
            <Description>Connect to Marketplace to see your{'\n'}profiled ads, including secured warranty{'\n'}check and ownership history</Description>
          </DescContainer>
          <ActionsContainer>
            <Description style={{ fontFamily: 'gotham-medium', marginBottom: 45, }}>Select Marketplace you want to{'\n'}publish your product to sell</Description>
            <MarketplaceCard>
              <View>
                <Image
                  source = {require('./../../../assets/images/Carousell.png')}
                  style  = {{alignSelf: 'center', width: 180, height: 50, resizeMode: 'contain'}}
                />
              </View>
              <View
                style = {{ paddingHorizontal: 5, paddingVertical: 5, borderColor: '#979797', borderWidth: 1, borderRadius: 30,  width: 78, height: 37 }}>
                <Switch 
                  defaultValue = {true}
                  width        = {68}
                  height       = {27}
                  // circleStyle  = {{width: 25, height: 25.}}
                  backgroundActive    = {'transparent'}
                  backgroundInactive  = {'transparent'}
                  circleColorActive   = {'#4A90E2'}
                  circleColorInactive = {'#B5B5B5'}
                />
              </View>
            </MarketplaceCard>
            <MarketplaceCard>
              <View>
                <Image
                  source = {require('./../../../assets/images/Lelong.png')}
                  style  = {{alignSelf: 'center', width: 180, height: 50, resizeMode: 'contain'}}
                />
              </View>
              <View
                style = {{ paddingHorizontal: 5, paddingVertical: 5, borderColor: '#979797', borderWidth: 1, borderRadius: 30,  width: 78, height: 37 }}>
                <Switch
                  defaultValue
                  width        = {68}
                  height       = {27}
                  // circleStyle  = {{width: 25, height: 25.}}
                  backgroundActive    = "transparent"
                  backgroundInactive  = "transparent"
                  circleColorActive   = "#4A90E2"
                  circleColorInactive = "#B5B5B5"
                />
              </View>
            </MarketplaceCard>
            <MarketplaceCard>
              <View>
                <Image
                  source = {require('./../../../assets/images/Lowyat.png')}
                  style  = {{alignSelf: 'center', width: 180, height: 50, resizeMode: 'contain'}}
                />
              </View>
              <View
                style = {{ paddingHorizontal: 5, paddingVertical: 5, borderColor: '#979797', borderWidth: 1, borderRadius: 30,  width: 78, height: 37 }}>
                <Switch
                  defaultValue
                  width        = {68}
                  height       = {27}
                  // circleStyle  = {{width: 25, height: 25.}}
                  backgroundActive    = {'transparent'}
                  backgroundInactive  = {'transparent'}
                  circleColorActive   = {'#4A90E2'}
                  circleColorInactive = {'#B5B5B5'}
                />
              </View>
            </MarketplaceCard>
            <MarketplaceCard>
              <View>
                <Image
                  source = {require('./../../../assets/images/Mudah.png')}
                  style  = {{alignSelf: 'center', width: 180, height: 50, resizeMode: 'contain'}}
                />
              </View>
              <View
                style = {{ paddingHorizontal: 5, paddingVertical: 5, borderColor: '#979797', borderWidth: 1, borderRadius: 30,  width: 78, height: 37 }}>
                <Switch
                  defaultValue
                  width        = {68}
                  height       = {27}
                  // circleStyle  = {{width: 25, height: 25.}}
                  backgroundActive    = {'transparent'}
                  backgroundInactive  = {'transparent'}
                  circleColorActive   = {'#4A90E2'}
                  circleColorInactive = {'#B5B5B5'}
                />
              </View>
            </MarketplaceCard>
          </ActionsContainer>
        </Container>
      );
    }

    return (
      <Container>
        <ComingSoonTitle>COMING SOON</ComingSoonTitle>
        <ComingSoonDescription>Automatically post your stuff to major{'\n'}marketplaces of your choice and easily{'\n'}transfer warranty once sold</ComingSoonDescription>
        <ComingSoonSubtitle>Choose to sell at any time with{'\n'}just a swipe on the screen</ComingSoonSubtitle>
      </Container>
    );
  }
}

export default Scene;