import React from 'react';
import { StackNavigator } from 'react-navigation';
import LandingScreen from 'components/screens/LandingScreen';
import LoginScreen from 'components/screens/LoginScreen';
import WelcomeScreen from 'components/screens/WelcomeScreen';

const LoginStackNavigator = StackNavigator(
  {
    Landing: { screen: LandingScreen },
    Login  : { screen: LoginScreen },
    Welcome: { screen: WelcomeScreen },
  },
  {
    initialRouteName: 'Landing',
    headerMode      : 'none',
  },
);

export default LoginStackNavigator;