import { StackNavigator } from 'react-navigation';

import LandingScreen from 'components/screens/LandingScreen';
import LoginScreen from 'components/screens/LoginScreen';
import WelcomeScreen from 'components/screens/WelcomeScreen';
import HomeScreen from 'components/screens/HomeScreen';
import MenuScreen from 'components/screens/MenuScreen';
import ProductDetailsScreen from 'components/screens/ProductDetailsScreen';
import UploadMethodScreen from 'components/screens/UploadMethodScreen';
import QRCodeScreen from 'components/screens/QRCodeScreen';
import ScanReceiptScreen from 'components/screens/ScanReceiptScreen';
import ManualUploadScreen from 'components/screens/ManualUploadScreen';
import UploadSuccessScreen from 'components/screens/UploadSuccessScreen';
import ManualUploadSuccessScreen from 'components/screens/ManualUploadSuccessScreen';
import ContactUsScreen from 'components/screens/ContactUsScreen';

const CoreStackNavigator = StackNavigator(
  {
    Landing            : { screen: LandingScreen },
    Login              : { screen: LoginScreen },
    Welcome            : { screen: WelcomeScreen },
    Home               : { screen: HomeScreen },
    Menu               : { screen: MenuScreen },
    ProductDetails     : { screen: ProductDetailsScreen },
    UploadMethod       : { screen: UploadMethodScreen },
    QRCode             : { screen: QRCodeScreen },
    ScanReceipt        : { screen: ScanReceiptScreen },
    ManualUpload       : { screen: ManualUploadScreen },
    UploadSuccess      : { screen: UploadSuccessScreen },
    ManualUploadSuccess: { screen: ManualUploadSuccessScreen },
    ContactUs          : { screen: ContactUsScreen },
  },
  {
    initialRouteName: 'Landing',
    headerMode      : 'none',
  },
);

export default CoreStackNavigator;
