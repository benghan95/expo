export const Colors = {
  Default: {
    Cyan     : '#139AD6',
    DarkCyan : '#0D658D',
    Teal     : '#0E6D97',
    Blue     : '#234E7F',
    Green    : '#4DE24A',
    DarkGreen: '#0D8D65',
    Red : '#F87676',
  },
};
