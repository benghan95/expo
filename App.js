import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { StackNavigator, DrawerNavigator, DrawerItems } from 'react-navigation';
import { Font } from 'expo';
import LoginStackNavigator from './src/components/navigation/LoginStackNavigator';
import CoreStackNavigator from './src/components/navigation/CoreStackNavigator';
import styled from "styled-components";

const DrawerContainer = styled.View`
  flex: 1;
`;

const AppContainer = styled.View`
  flex: 1;
`;

const drawerRouteConfig = {
  Login: {
    screen: LoginStackNavigator,
  },
  Core: {
    screen: CoreStackNavigator,
  }
};

const drawerNavigatorConfig = {
};

const Stack = StackNavigator(
  {
    Login: {
        screen: LoginStackNavigator,
    },
  },
  {
    initialRouteName: 'Login',
    headerMode      : 'none',
  },
);

const AppDrawer = DrawerNavigator(drawerRouteConfig, drawerNavigatorConfig);

export default class App extends Component {
  state = {
    fontLoaded: false,
  };

  async componentDidMount() {
    await Font.loadAsync({
      'gotham-bold'  : require('./assets/fonts/Gotham-Bold.ttf'),
      'gotham-medium': require('./assets/fonts/Gotham-Medium.otf'),
      'gotham-book'  : require('./assets/fonts/Gotham-Book.otf'),
    })

    this.setState({ fontLoaded: true });
  }

  render() {
    return (
      this.state.fontLoaded ? (
        <AppContainer>
          <StatusBar hidden={true} />
          <CoreStackNavigator />
        </AppContainer>
      ) : null
    );
  }
}